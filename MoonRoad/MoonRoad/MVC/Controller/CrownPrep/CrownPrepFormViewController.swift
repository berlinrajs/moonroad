//
//  CrownPrepFormViewController.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 01/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class CrownPrepFormViewController: PDViewController {

    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet var buttonsQuestion: [UIButton]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for button in buttonsQuestion {
            button.selected = patient.crownPrepCheckList[button.tag - 1].selectedOption == true
        }
        labelName.text = patient.fullName
        signaturePatient.image = patient.crownPrepSignature
        labelDate1.text = patient.dateToday
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
