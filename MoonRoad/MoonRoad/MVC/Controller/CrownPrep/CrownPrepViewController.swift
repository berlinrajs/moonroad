//
//  CrownPrepViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 7/1/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class CrownPrepViewController: PDViewController {

    @IBOutlet weak var tableView: YNTableView!
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var buttonVerified: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate.todayDate = patient.dateToday
        
        if patient.crownPrepCheckList == nil {
            let questions = ["Take Blood Pressure", "Pre-Op Medications Dispensed", "Medical History reviewed", "Give Consent form to Pt and make sure it’s read, answered all pt’s questions and signed", "Take shade for the crown and get pt’s signature", "Use Check Retractors for Pictures (smileline)", "Take alginate impression (smileline X2)", "High Speed hand piece ready with Round End diamond on it", "Basket w/ Triple tray blue mousse and cinch material and Tips", "Get Lab Slip ready with Name, Shade", "Ziploc bag ready for blue mousse & cinch impressions. Bite registration", "Box with Label from labtech", "Post-Op Instructions", "Contact delivery service (UPS or DHL)"]
            
            patient.crownPrepCheckList = PDQuestion.arrayOfQuestions(questions)
        }
        tableView.cellType = YNCellType.CheckBox
        tableView.ynDataSource = self
        // Do any additional setup after loading the view.
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonVerifiedAction(sender: UIButton) {
        sender.selected = !sender.selected
    }
    @IBAction func buttonNextAction() {
        if !buttonVerified.selected {
            self.showAlert("PEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
        } else if !signatureView.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        } else {
            patient.crownPrepSignature = signatureView.signatureImage()
            
            let new1VC = self.storyboard!.instantiateViewControllerWithIdentifier("kCrownPrepFormViewController") as! CrownPrepFormViewController
//            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
    }
}
extension CrownPrepViewController: YNTableViewDataSource {
    func sectionTitlesInTableView(tableView: YNTableView) -> [String]? {
        return nil
    }
    func arrayOfQuestionsInTableView(tableView: YNTableView) -> [[PDQuestion]]? {
        return [patient.crownPrepCheckList]
    }
}
