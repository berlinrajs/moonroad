//
//  OralSedationViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 7/4/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class OralSedationViewController: PDViewController {
    @IBOutlet weak var textfieldDriverName : UITextField!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var signatureWitness : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (sender : UIButton){
        if textfieldDriverName.isEmpty {
            self.showAlert("PLEASE ENTER THE DRIVER NAME")
        }else if !signatureWitness.isSigned() || !signaturePatient.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            patient.oralSedationDriverName = textfieldDriverName.text!
            patient.oralSedationPatientSignature = signaturePatient.signatureImage()
            patient.oralSedationWitnessSignature = signatureWitness.signatureImage()
            let new1VC = newStoryBoard.instantiateViewControllerWithIdentifier("OralSedationFormVC") as! OralSedationFormViewController
//            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)

        }
    }
}

extension OralSedationViewController : UITextFieldDelegate{
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
