//
//  OralSedationFormViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 7/4/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class OralSedationFormViewController: PDViewController {
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var labelDriverName : UILabel!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var signatureWitness : UIImageView!
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDateBirth: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelName.text = patient.fullName
        labelDateBirth.text = patient.dateOfBirth
        
        labelDate.text = patient.dateToday
        labelDriverName.text = patient.oralSedationDriverName
        signaturePatient.image = patient.oralSedationPatientSignature
        signatureWitness.image = patient.oralSedationWitnessSignature
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
