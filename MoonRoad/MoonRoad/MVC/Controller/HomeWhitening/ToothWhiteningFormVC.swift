//
//  ToothWhiteningFormVC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 07/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ToothWhiteningFormVC: PDViewController {

    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var signatureImage: UIImageView!
    @IBOutlet weak var labelHour1: UILabel!
    @IBOutlet weak var labelHour2: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate.text = patient.dateToday
        labelHour1.text = patient.selectedForms.first!.toothNumbers
        labelHour2.text = patient.selectedForms.first!.additionalDetails
        signatureImage.image = patient.teethWhiteningSignature

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
