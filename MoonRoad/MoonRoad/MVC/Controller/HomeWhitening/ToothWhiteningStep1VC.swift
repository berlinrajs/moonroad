//
//  HomeWhitening1VC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 07/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ToothWhiteningStep1VC: PDViewController {

    @IBOutlet weak var patientSignatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var labelDetails: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDetails.text = labelDetails.text?.stringByReplacingOccurrencesOfString("HOUR_1", withString: patient.selectedForms.first!.toothNumbers)
        labelDetails.text = labelDetails.text?.stringByReplacingOccurrencesOfString("HOUR_2", withString: patient.selectedForms.first!.additionalDetails)
        labelDate.todayDate = patient.dateToday
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonNextAction() {
        if !self.patientSignatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        } else {
            patient.teethWhiteningSignature = patientSignatureView.signatureImage()
            
            let formVC = self.storyboard!.instantiateViewControllerWithIdentifier("kToothWhiteningFormVC") as! ToothWhiteningFormVC
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
}
