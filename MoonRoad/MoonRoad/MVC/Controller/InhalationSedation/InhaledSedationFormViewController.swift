//
//  InhaledSedationFormViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 7/1/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class InhaledSedationFormViewController: PDViewController {

    @IBOutlet weak var labelName: UILabel!
    
    var signedBy: String!
    
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var signatureWitness : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        signaturePatient.image = patient.inhalationSignaturePatient
        signatureWitness.image = patient.inhalationSignatureWitness
        labelDate1.text = patient.dateToday
        labelName.text = signedBy

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
