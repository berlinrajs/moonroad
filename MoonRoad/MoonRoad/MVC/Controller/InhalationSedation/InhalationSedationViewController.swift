//
//  InhalationSedationViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 7/1/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class InhalationSedationViewController: PDViewController {

    @IBOutlet weak var signature : SignatureView!
    @IBOutlet weak var signatureWitness : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var textFieldName: PDTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        textFieldName.text = patient.fullName

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (sender : UIButton){
        if !signature.isSigned() || !signatureWitness.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            patient.inhalationSignaturePatient = signature.signatureImage()
            patient.inhalationSignatureWitness = signatureWitness.signatureImage()
            let new1VC = self.storyboard!.instantiateViewControllerWithIdentifier("InhalationSedationFormVC") as! InhaledSedationFormViewController
            new1VC.signedBy = textFieldName.text!
//            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
    }


}
extension InhalationSedationViewController: UITextFieldDelegate {
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldName {
            return textField.formatFullName(range, string: string)
        }
        return true
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
