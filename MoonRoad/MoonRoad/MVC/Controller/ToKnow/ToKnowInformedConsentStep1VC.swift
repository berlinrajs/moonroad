//
//  ToKnowInformedConsentStep1VC.swift
//  MoonRoad
//
//  Created by Berlin Raj on 23/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ToKnowInformedConsentStep1VC: PDViewController {

    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var textFieldName: PDTextField!
    @IBOutlet weak var witnessSignatureView: SignatureView!
    @IBOutlet weak var textViewNotes: PDTextView!
    @IBOutlet weak var labelPatientName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate.todayDate = patient.dateToday
        labelName.text = patient.fullName
        labelPatientName.text = "For the orthodontic treatment of \(patient.fullName)"
        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        if !signatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !witnessSignatureView.isSigned() {
                self.showAlert("PLEASE GET SIGN FROM WITNESS")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE TAP THE DATE")
        } else {
            let formVC = self.storyboard?.instantiateViewControllerWithIdentifier("kToKnowInformedConsentFormVC") as! ToKnowInformedConsentFormVC
            formVC.signature = signatureView.signatureImage()
            formVC.signedName = labelName.text!
            formVC.witnessSignature = witnessSignatureView.signatureImage()
            formVC.notes = textViewNotes.text == "NOTES" ? "" : textViewNotes.text!
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension ToKnowInformedConsentStep1VC: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldName {
            let newRange = textField.text!.startIndex.advancedBy(range.location)..<textField.text!.startIndex.advancedBy(range.location + range.length)
            let newName = textField.text!.stringByReplacingCharactersInRange(newRange, withString: string)
            
            labelName.text = newName.characters.count > 0 ? newName : patient.fullName
        }
        return true
    }
}

extension ToKnowInformedConsentStep1VC: UITextViewDelegate {
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text == "" {
            textView.text = "NOTES"
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.text == "NOTES" {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            return textView.resignFirstResponder()
        }
        return true
    }
}
