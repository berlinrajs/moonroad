//
//  ToKnowInformedConsentFormVC.swift
//  MoonRoad
//
//  Created by Berlin Raj on 23/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ToKnowInformedConsentFormVC: PDViewController {

    var signedName: String!
    var signature: UIImage!
    var witnessSignature: UIImage!
    var notes: String!
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var signatureView: UIImageView!
    @IBOutlet weak var signatureViewWitness: UIImageView!
    @IBOutlet weak var labelPatientName1: UILabel!
    @IBOutlet weak var labelPatientName2: UILabel!
    @IBOutlet weak var labelSignedName: UILabel!
    @IBOutlet weak var labelDOB: UILabel!
    @IBOutlet weak var labelNotes1: UILabel!
    @IBOutlet weak var labelNotes2: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        labelDate.text = patient.dateToday
        signatureView.image = signature
        labelPatientName1.text = patient.fullName
        labelPatientName2.text = patient.fullName
        labelSignedName.text = signedName != patient.fullName ? signedName : ""
        signatureViewWitness.image = witnessSignature
        labelDOB.text = patient.dateOfBirth
        notes.setTextForArrayOfLabels([labelNotes1, labelNotes1])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
