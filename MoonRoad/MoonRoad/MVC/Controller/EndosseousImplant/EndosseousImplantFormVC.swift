//
//  EndosseousImplantFormVC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 04/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class EndosseousImplantFormVC: PDViewController {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDateOfBirth: UILabel!
    
    @IBOutlet weak var patientSignature: UIImageView!
    @IBOutlet weak var dentistSignature: UIImageView!
    @IBOutlet weak var witnessSignature: UIImageView!
    @IBOutlet weak var radioLiklihood: RadioButton!
    @IBOutlet weak var labelParentName: UILabel!
    @IBOutlet weak var labelReason1: UILabel!
    @IBOutlet weak var labelReason2: UILabel!
    @IBOutlet weak var labelRelation: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    @IBOutlet weak var patietnInitial1: UIImageView!
    @IBOutlet weak var patietnInitial2: UIImageView!
    @IBOutlet weak var patietnInitial3: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        patientSignature.image = patient.endosseousImplantPatientSign
        dentistSignature.image = patient.endosseousImplantDentistSign
        witnessSignature.image = patient.endosseousImplantWitnessSign
        radioLiklihood.setSelectedWithTag(patient.endosseousImplantLiklihoodSelectedOption)
        labelParentName.text = patient.endosseousImplantRelationName
        patient.endosseousImplantReason.setTextForArrayOfLabels([labelReason1, labelReason2])
        labelRelation.text = patient.endosseousImplantRelationShip
        labelDate.text = patient.dateToday
        labelName.text = patient.fullName
        patietnInitial1.image = patient.endosseousImplantPatientInitial
        patietnInitial2.image = patient.endosseousImplantPatientInitial
        patietnInitial3.image = patient.endosseousImplantPatientInitial
        labelDateOfBirth.text = patient.dateOfBirth
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
