//
//  EndosseousImplant.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 01/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class EndosseousImplantStep1VC: PDViewController {
    
    @IBOutlet weak var patientSignature: SignatureView!
    @IBOutlet weak var dentistSignature: UIImageView!
    @IBOutlet weak var witnessSignature: SignatureView!
    @IBOutlet weak var patientInitial: SignatureView!
    @IBOutlet weak var radioLiklihood: RadioButton!
    @IBOutlet weak var textFieldParentName: PDTextField!
    @IBOutlet weak var textFieldReason: PDTextField!
    @IBOutlet weak var textFieldRelation: PDTextField!
    @IBOutlet weak var labelDate: DateLabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldParentName.text = patient.fullName
        labelDate.todayDate = patient.dateToday
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonNextAction() {
        if radioLiklihood.selectedButton == nil {
            self.showAlert("PLEASE SELECT AN OPTION ABOUT THE LIKELIHOOD SUCCESS OF THE PROCEDURE")
            self.scrollView.setContentOffset(CGPointMake(0, 0), animated: true)
        } else if !patientSignature.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !patientInitial.isSigned() {
            self.showAlert("PLEASE PUT THE INITIAL")
        } else if !witnessSignature.isSigned() {
            self.showAlert("PLEASE GET THE SIGNATURE FROM WITNESS")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        } else if textFieldParentName.isEmpty {
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        } else if !textFieldRelation.isEmpty && textFieldReason.isEmpty {
            self.showAlert("PLEASE ENTER THE REASON PATIENT COULD NOT SIGN")
        } else if textFieldRelation.isEmpty && !textFieldReason.isEmpty {
            self.showAlert("PLEASE SPECIFY THE RELATIONSHIP TO PATIENT")
        } else {
            patient.endosseousImplantPatientSign = patientSignature.signatureImage()
            patient.endosseousImplantPatientInitial = patientInitial.signatureImage()
            patient.endosseousImplantWitnessSign = witnessSignature.signatureImage()
            patient.endosseousImplantDentistSign = dentistSignature.image
            patient.endosseousImplantRelationShip = textFieldRelation.isEmpty ? "" : textFieldRelation.text!
            patient.endosseousImplantRelationName = textFieldParentName.isEmpty ? "" : textFieldParentName.text!
            patient.endosseousImplantLiklihoodSelectedOption = radioLiklihood.selectedButton.tag
            patient.endosseousImplantReason = textFieldReason.isEmpty ? "" : textFieldReason.text!
            
            let formVC = self.storyboard?.instantiateViewControllerWithIdentifier("kEndosseousImplantFormVC") as! EndosseousImplantFormVC
//            formVC.patient = self.patient
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
}
extension EndosseousImplantStep1VC: UITextFieldDelegate {
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldParentName {
            return textField.formatFullName(range, string: string)
        }
        return true
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
