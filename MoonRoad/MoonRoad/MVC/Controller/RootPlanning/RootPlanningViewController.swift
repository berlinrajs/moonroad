//
//  RootPlanningViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 7/4/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class RootPlanningViewController: PDViewController {

    @IBOutlet weak var textfiedQuad : UITextField!
    @IBOutlet weak var patientSignatureView : SignatureView!
    @IBOutlet weak var witnessSignatureView : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    @IBAction func onNextButtonPressed (sender : UIButton){
        if !patientSignatureView.isSigned() || !witnessSignatureView.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }
        else{
            let new1VC = newStoryBoard.instantiateViewControllerWithIdentifier("RootPlanningFormVC") as! RootPlanningFormViewController
            new1VC.quad = textfiedQuad.isEmpty ? "" : textfiedQuad.text!
            new1VC.patientSignature = patientSignatureView.signatureImage()
            new1VC.witnessSignature = witnessSignatureView.signatureImage()
//            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)

        }
    }

}
