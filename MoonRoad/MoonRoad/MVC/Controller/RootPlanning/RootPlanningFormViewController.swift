//
//  RootPlanningFormViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 7/4/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class RootPlanningFormViewController: PDViewController {

    var quad : String!
    var patientSignature : UIImage!
    var witnessSignature : UIImage!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var labelQuad : UILabel!
    @IBOutlet weak var patientSignatureView : UIImageView!
    @IBOutlet weak var witnessSignatureView : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelPatientName.text = patient.fullName
        labelDate.text = patient.dateToday
        labelQuad.text = quad
        patientSignatureView.image = patientSignature
        witnessSignatureView.image = witnessSignature
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
