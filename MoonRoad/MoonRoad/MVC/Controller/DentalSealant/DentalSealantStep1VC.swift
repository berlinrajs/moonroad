//
//  DentalSealantStep1VC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 01/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class DentalSealantStep1VC: PDViewController {

    @IBOutlet weak var patientSignatureView: SignatureView!
//    @IBOutlet weak var dentistSignatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var signatureWitness: SignatureView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonNextAction() {
        if !patientSignatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !signatureWitness.isSigned() {
            self.showAlert("PLEASE GET SIGN FROM WITNESS")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        } else {
            patient.dentalSealantPatientSign = patientSignatureView.signatureImage()
            patient.dentalSealantWitness = signatureWitness.signatureImage()
            
            let formVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentalSealantFormVC") as! DentalSealantFormVC
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
}
