//
//  DentalSealantFormVC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 01/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class DentalSealantFormVC: PDViewController {

    @IBOutlet weak var patientSignatureView: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var signatureWitness: UIImageView!
    @IBOutlet weak var labelToothNumber: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        patientSignatureView.image = patient.dentalSealantPatientSign
        labelName.text = patient.fullName
        labelDate.text = patient.dateToday
        signatureWitness.image = patient.dentalSealantWitness
        labelToothNumber.text = patient.selectedForms.first?.toothNumbers
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
