//
//  ProstheticsTreatmentFormVC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 04/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ProstheticsTreatmentFormVC: PDViewController {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelShade: UILabel!
    @IBOutlet weak var labelToothNumber: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var patientSignatureView: UIImageView!
    @IBOutlet weak var witnessSignatureView: UIImageView!
    
    @IBOutlet var initialViews: [UIImageView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        for initial in initialViews {
            initial.image = patient.prostheticsInitials["\(initial.tag)"] as? UIImage
        }
        
        labelName.text = patient.fullName
        labelShade.text = patient.prostheticsTreatmentShade
        labelDate.text = patient.dateToday
        labelToothNumber.text = patient.prostheticsTreatmentToothNumber
        patientSignatureView.image = patient.prostheticsTreatmentSignature
        witnessSignatureView.image = patient.prostheticsTreatmentWitness
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
