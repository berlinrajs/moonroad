//
//  ProstheticsTreatmentStep1VC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 04/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ProstheticsTreatmentStep1VC: PDViewController {

    @IBOutlet weak var textFieldToothNumbers: PDTextField!
    @IBOutlet weak var textFieldShade: PDTextField!
    @IBOutlet weak var patientSignature: SignatureView!
    @IBOutlet weak var witnessSignature: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet var initialViews: [SignatureView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        textFieldShade.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var foundNil: Bool {
        get {
            for initial in initialViews {
                if !initial.isSigned() {
                    return true
                }
            }
            return false
        }
    }
    
    @IBAction func buttonNextAction() {
        if foundNil {
            self.showAlert("PLEASE SIGN THE MISSING INITIALS")
        } else if textFieldToothNumbers.isEmpty || textFieldShade.isEmpty {
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        } else if !patientSignature.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !witnessSignature.isSigned() {
            self.showAlert("PLEASE GET SIGNATURE FROM WITNESS")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        } else {
            patient.prostheticsInitials = NSMutableDictionary()
            for initial in initialViews {
                patient.prostheticsInitials.setValue(initial.signatureImage(), forKey: "\(initial.tag)")
            }
            
            patient.prostheticsTreatmentSignature = patientSignature.signatureImage()
            patient.prostheticsTreatmentWitness = witnessSignature.signatureImage()
            patient.prostheticsTreatmentToothNumber = textFieldToothNumbers.text!
            patient.prostheticsTreatmentShade = textFieldShade.text!
            
            let form = self.storyboard?.instantiateViewControllerWithIdentifier("kProstheticsTreatmentFormVC") as! ProstheticsTreatmentFormVC
            self.navigationController?.pushViewController(form, animated: true)
        }
    }
}
extension ProstheticsTreatmentStep1VC: UITextFieldDelegate {
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
//        if textField == textFieldToothNumbers {
//            return textField.formatToothNumbers(range, string: string)
//        }
        if textField == textFieldShade {
            return textField.formatShade(range, string: string)
        }
        return true
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
