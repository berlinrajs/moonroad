//
//  PreSedationFormVC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 04/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PreSedationFormVC: PDViewController {

    @IBOutlet weak var labelPatientName: UILabel!
    @IBOutlet weak var labelDateOfBirth: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var signatureView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        labelPatientName.text = patient.fullName
        labelDateOfBirth.text = patient.dateOfBirth
        labelDate.text = patient.dateToday
        signatureView.image = patient.preSedationSignature
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
