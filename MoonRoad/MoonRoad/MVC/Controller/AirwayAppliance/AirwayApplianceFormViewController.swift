//
//  AirwayApplianceFormViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 6/30/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class AirwayApplianceFormViewController: PDViewController {

    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var signatureDoctor : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var viewContainer : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        signaturePatient.image = patient.airwaySignaturePatient
        signatureDoctor.image = patient.airwaySignatureDoctor
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        viewContainer.bringSubviewToFront(signaturePatient)
        viewContainer.bringSubviewToFront(signatureDoctor)
        viewContainer.bringSubviewToFront(labelDate1)
        viewContainer.bringSubviewToFront(labelDate2)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
