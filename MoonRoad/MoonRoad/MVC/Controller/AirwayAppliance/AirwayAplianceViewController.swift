//
//  AirwayAplianceViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 6/30/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class AirwayAplianceViewController: PDViewController {
    @IBOutlet weak var signature : SignatureView!
    @IBOutlet weak var signatureDoctor : UIImageView!
    @IBOutlet weak var labelDate : DateLabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (sender : UIButton){
        if !signature.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            patient.airwaySignaturePatient = signature.signatureImage()
            patient.airwaySignatureDoctor = signatureDoctor.image
            let new1VC = self.storyboard!.instantiateViewControllerWithIdentifier("AirwayFormVC") as! AirwayApplianceFormViewController
//            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
    }


}
