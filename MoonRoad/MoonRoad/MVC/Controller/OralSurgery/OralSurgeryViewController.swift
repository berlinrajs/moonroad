//
//  OralSurgeryViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 7/4/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class OralSurgeryViewController: PDViewController {
    
    @IBOutlet weak var signatureDriver : SignatureView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onNextButtonPressed (sender : UIButton){
        if  !signatureDriver.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        }else{
            let new1VC = newStoryBoard.instantiateViewControllerWithIdentifier("OralSurgeryFormVC") as! OralSurgeryFormViewController
            new1VC.driverSignature = signatureDriver.signatureImage()
//            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)

        }
        
    }

}
