//
//  OralSurgeryFormViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 7/4/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class OralSurgeryFormViewController: PDViewController {

    var driverSignature : UIImage!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var driverSignatureView : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelPatientName.text = patient.fullName
        driverSignatureView.image = driverSignature
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
