//
//  MouthGaurdFormViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 7/4/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class MouthGaurdFormViewController: PDViewController {

    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var parentSignature : UIImageView!
    @IBOutlet weak var witnessSignature : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelPatientName.text = patient.fullName
        parentSignature.image = patient.mouthGaurdParentSignature
        witnessSignature.image = patient.mouthGaurdWitnessSignature

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
