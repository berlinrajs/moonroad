//
//  MouthGaurdViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 7/4/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class MouthGaurdViewController: PDViewController {

    @IBOutlet weak var signatureWitness : SignatureView!
    @IBOutlet weak var signatureParent : SignatureView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onNextButtonPressed (sender : UIButton){
        if !signatureWitness.isSigned() || !signatureParent.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        }else{
            patient.mouthGaurdParentSignature = signatureParent.signatureImage()
            patient.mouthGaurdWitnessSignature = signatureWitness.signatureImage()
            let new1VC = newStoryBoard.instantiateViewControllerWithIdentifier("MouthGaurdFormVC") as! MouthGaurdFormViewController
//            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)

        }
    }

}
