//
//  TemporomandibularFormVC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 05/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class TemporomandibularFormVC: PDViewController {

    @IBOutlet weak var labelDate1: UILabel!
    @IBOutlet weak var labelDate2: UILabel!
    @IBOutlet weak var patientSignature: UIImageView!
    @IBOutlet weak var witnessSignature: UIImageView!
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDateOfBirth: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        patientSignature.image = patient.temporomandibularPatientSign
        witnessSignature.image = patient.temporomandibularWitnessSign
        labelName.text = patient.fullName
        labelDateOfBirth.text = patient.dateOfBirth
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
