//
//  TemporomandibularStep1VC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 05/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class TemporomandibularStep1VC: PDViewController {

    @IBOutlet weak var labelDate1: DateLabel!
    @IBOutlet weak var labelDate2: DateLabel!
    @IBOutlet weak var patientSignature: SignatureView!
    @IBOutlet weak var witnessSignature: SignatureView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonNextAction() {
        if !patientSignature.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate1.dateTapped || !labelDate2.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        } else if !witnessSignature.isSigned() {
            self.showAlert("PLEASE GET SIGNATURE FROM WITNESS")
        } else {
            patient.temporomandibularPatientSign = patientSignature.signatureImage()
            patient.temporomandibularWitnessSign = witnessSignature.signatureImage()
            
            let formVC = self.storyboard?.instantiateViewControllerWithIdentifier("kTemporomandibularFormVC") as! TemporomandibularFormVC
//            formVC.patient = self.patient
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
}
