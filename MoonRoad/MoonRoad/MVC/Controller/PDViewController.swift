//
//  PDViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

var patient: PDPatient!

class PDViewController: UIViewController {
    
    @IBOutlet var buttonSubmit: PDButton?
    @IBOutlet var buttonBack: PDButton?
    
    @IBOutlet var pdfView: UIScrollView?
    let mainStoryBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let newStoryBoard : UIStoryboard = UIStoryboard(name: "New", bundle: nil)

    var isFromPreviousForm: Bool {
        get {
            return navigationController?.viewControllers.count > 3 ? true : false
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = .None
        buttonBack?.hidden = isFromPreviousForm && buttonSubmit == nil

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func buttonBackAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func onSubmitButtonPressed (sender : UIButton){
        if !Reachability.isConnectedToNetwork() {
            let alertController = UIAlertController(title: "MOON ROAD DENTISTRY", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.Alert)
            let alertOkAction = UIAlertAction(title: "SETTINGS", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
            let alertCancelAction = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                
            }
            alertController.addAction(alertOkAction)
            alertController.addAction(alertCancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
            return
        }
        let pdfManager = PDFManager()
        pdfManager.authorizeDrive(self) { (success) -> Void in
            if success {
                self.buttonSubmit?.hidden = true
                self.buttonBack?.hidden = true
                if self.pdfView != nil {
                    pdfManager.createPDFForScrollView(self.pdfView!, patient: patient, completionBlock: { (finished) -> Void in
                        if finished {
                            if patient.selectedForms.first!.formTitle! == "NOTICE OF PRIVACY PRACTICES"{
                                patient.selectedForms.removeFirst()
                            }
                            self.gotoNextForm(false)
                        } else {
                            self.buttonSubmit?.hidden = false
                            self.buttonBack?.hidden = false
                        }
                    })
                } else {
                    pdfManager.createPDFForView(self.view, patient: patient, completionBlock: { (finished) -> Void in
                        if finished {
                            self.gotoNextForm(false)
                        } else {
                            self.buttonSubmit?.hidden = false
                            self.buttonBack?.hidden = false
                        }
                    })
                }
                
            } else {
                self.buttonSubmit?.hidden = false
                self.buttonBack?.hidden = false
            }
        }
    }
    
    func gotoNextForm(showBackButton : Bool) {
        if isFromPreviousForm {
            if patient.selectedForms.count > 0 { patient.selectedForms.removeFirst() }
        }
        let formNames = (patient.selectedForms as NSArray).valueForKey("formTitle") as! [String]
        if formNames.contains(kAirwayAppliance){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("Airway1VC") as! AirwayAplianceViewController
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(kBridgeAwareness){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("BridgeVC") as! BridgeAwarenessViewController
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(kCrownPrep){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("kCrownPrepViewController") as! CrownPrepViewController
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(kBoneGraft){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("kBoneGraftInformedConsent1VC") as! BoneGraftInformedConsent1VC
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        }  else if formNames.contains(kDentalSealant){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("kDentalSealantStep1VC") as! DentalSealantStep1VC
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(kEndosseousImplant){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("kEndosseousImplantStep1VC") as! EndosseousImplantStep1VC
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(kRootCanal) {
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("kRootCanal1ViewController") as! RootCanal1ViewController
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kRestorativeTreatmment){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("RestorativeVC") as! RestorativeTreatmentViewController
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kIntravenousSedation){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("IntravenousVC") as! IntravenousTreatmentViewController
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kToothRemoval){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("ToothRemovalVC") as! ToothRemovalViewController
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kInhaledSedation){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("InhalationSedationVC") as! InhalationSedationViewController
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(kHomeCare){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("HomeCareVC") as! HomeCareViewController
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(kOrthodonticService){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("kOrthodonticServiceStep1VC") as! OrthodonticServiceStep1VC
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(kProstheticsTreatment){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("kProstheticsTreatmentStep1VC") as! ProstheticsTreatmentStep1VC
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(kRetainerWear){
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("kRetainerWearStep1VC") as! RetainerWearStep1VC
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(kPreSedation) {
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("kPreSedationStep1VC") as! PreSedationStep1VC
//            new1VC.patient = patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(kCheckList) {
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("kCheckListStep1VC") as! CheckListStep1VC
            //            new1VC.patient = patient
            self.pushToViewController(new1VC)
            //            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kMouthGaurd){
            let new1VC = newStoryBoard.instantiateViewControllerWithIdentifier("MouthGaurdVC") as! MouthGaurdViewController
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kOralSedation){
            let new1VC = newStoryBoard.instantiateViewControllerWithIdentifier("OralSedationVC") as! OralSedationViewController
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kOralSurgery){
            let new1VC = newStoryBoard.instantiateViewControllerWithIdentifier("OralSurgeryVC") as! OralSurgeryViewController
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kBracesInstructions){
            let new1VC = newStoryBoard.instantiateViewControllerWithIdentifier("BracesInstructionVC") as! BracesInstructionViewController
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kRootPlanning){
            let new1VC = newStoryBoard.instantiateViewControllerWithIdentifier("RootPlanningVC") as! RootPlanningViewController
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kPeriodontalTreatment){
            let new1VC = newStoryBoard.instantiateViewControllerWithIdentifier("PeriodontalVC") as! PeriodontalViewController
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(kReline) {
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("kConsentRelineStep1VC") as! ConsentRelineStep1VC
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(kProsthodontic) {
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("kProsthondonticTreatmentStep1VC") as! ProsthondonticTreatmentStep1VC
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(kToothRemovalPrimary) {
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("kToothRemovalPrimaryStep1VC") as! ToothRemovalPrimaryStep1VC
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(kTemporomandibular) {
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("kTemporomandibularStep1VC") as! TemporomandibularStep1VC
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kCompleteVisit){
            let new1VC = newStoryBoard.instantiateViewControllerWithIdentifier("CompleteVisitVC") as! CompleteVisitViewController
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        }else if formNames.contains(kSurgicalProcedure){
            let new1VC = newStoryBoard.instantiateViewControllerWithIdentifier("SurgicalVC") as! SurgicalProcedureViewController
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(kHomeWhitening) {
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("kToothWhiteningStep1VC") as! ToothWhiteningStep1VC
//            new1VC.patient = self.patient
            self.pushToViewController(new1VC)
//            self.navigationController?.pushViewController(new1VC, animated: true)
        } else if formNames.contains(kEtiology) {
            let new1VC = mainStoryBoard.instantiateViewControllerWithIdentifier("kEtiologyStep1VC") as! EtiologyStep1VC
            self.pushToViewController(new1VC)
        } else if formNames.contains(kGingivectomy) {
            let new1VC = newStoryBoard.instantiateViewControllerWithIdentifier("kGingivectomyStep1VC") as! GingivectomyStep1VC
            self.pushToViewController(new1VC)
        } else if formNames.contains(kToKnow) {
            let new1VC = newStoryBoard.instantiateViewControllerWithIdentifier("kToKnowInformedConsentStep1VC") as! ToKnowInformedConsentStep1VC
            self.pushToViewController(new1VC)
        } else {
            NSNotificationCenter.defaultCenter().postNotificationName(kFormsCompletedNotification, object: nil)
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
    }
    func pushToViewController(viewController: UIViewController) {
        self.navigationController?.delegate = self
        if isFromPreviousForm {
            self.navigationController?.viewControllers.insert(viewController, atIndex: 3)
            self.navigationController?.popToViewController(viewController, animated: true)
        } else {
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}
extension PDViewController: UINavigationControllerDelegate {
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        navigationController.delegate = nil
        return PushTransition()
    }
}
class PushTransition: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.3
    }
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
        transitionContext.containerView().addSubview(toViewController!.view)
        transitionContext.containerView().bringSubviewToFront(toViewController!.view)
        toViewController!.view.frame.origin.x = screenSize.width
        
        let fromVC = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)
        
        UIView.animateWithDuration(self.transitionDuration(transitionContext), animations: {
            toViewController!.view.frame.origin.x = 0
            fromVC?.view.frame.origin.x = -screenSize.width/3
            }) { (finished) in
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
        }
    }
}
