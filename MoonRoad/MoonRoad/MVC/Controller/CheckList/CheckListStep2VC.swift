//
//  CheckListStep2VC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 07/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class CheckListStep2VC: PDViewController {

    var checkList: CheckList!
    
    @IBOutlet weak var radioAppointment: RadioButton!
    @IBOutlet weak var textFieldConsentForms: PDTextField!
    @IBOutlet weak var textFieldGivenDate: PDTextField!
    @IBOutlet weak var textFieldGivenTo: PDTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        DateInputView.addDatePickerForTextField(textFieldGivenDate, minimumDate: nil, maximumDate: nil)
        DateInputView.addDatePickerForTextField(textFieldConsentForms, minimumDate: nil, maximumDate: nil)
        
        checkList.completedForms = [Int]()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonNextAction() {
        if radioAppointment.selectedButton == nil {
            self.showAlert("PLEASE SELECT APPOINTMENT CONFIRMED OR NOT")
        } else if textFieldConsentForms.isEmpty || textFieldGivenTo.isEmpty || textFieldGivenDate.isEmpty {
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        } else {
            self.checkList.appointmentConfirmed = radioAppointment.selected
            self.checkList.formsCompletedDate = textFieldConsentForms.text!
            self.checkList.givenDate = textFieldGivenDate.text!
            self.checkList.givenToWhom = textFieldGivenTo.text!
            
            let step3VC = self.storyboard?.instantiateViewControllerWithIdentifier("kCheckListStep3VC") as! CheckListStep3VC
            step3VC.checkList = checkList
            self.navigationController?.pushViewController(step3VC, animated: true)
        }
    }
    @IBAction func buttonActionCompletedForms(sender: UIButton) {
        sender.selected = !sender.selected
        
        if sender.selected {
            if !checkList.completedForms.contains(sender.tag) {
                checkList.completedForms.append(sender.tag)
            }
        } else {
            if checkList.completedForms.contains(sender.tag) {
                checkList.completedForms.removeAtIndex(checkList.completedForms.indexOf(sender.tag)!)
            }
        }
    }
}
