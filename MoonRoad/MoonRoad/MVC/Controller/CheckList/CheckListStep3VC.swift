//
//  CheckListStep3VC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 07/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class CheckListStep3VC: PDViewController {

    var checkList: CheckList!
    
    @IBOutlet weak var radioRestRoom: RadioButton!
    @IBOutlet weak var radioOptions: RadioButton!
    @IBOutlet weak var radioSedation: RadioButton!
    @IBOutlet weak var radioBP: RadioButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonNextAction() {
        if radioRestRoom.selectedButton == nil || radioBP.selectedButton == nil || radioSedation.selectedButton == nil {
            self.showAlert("PLEASE SELECT ALL THE REQUIRED FIELDS")
        } else {
            checkList.usedRestRoom = radioRestRoom.selected
            checkList.dateCheckListOption = radioOptions.selectedButton == nil ? 0 : radioOptions.selectedButton.tag
            checkList.formFilledOutCompletely = radioSedation.selected
            checkList.slipStappedToSedationForm = radioBP.selected
            
            let formVC = self.storyboard?.instantiateViewControllerWithIdentifier("kCheckListFormVC") as! CheckListFormVC
            formVC.checkList = checkList
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
}
