//
//  CheckListStep1VC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 04/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class CheckListStep1VC: PDViewController {

    @IBOutlet weak var radioPlanReviewed: RadioButton!
    @IBOutlet weak var radioXrays: RadioButton!
    @IBOutlet weak var textFieldAuthSent: PDTextField!
    @IBOutlet weak var textFieldAuthReceived: PDTextField!
    @IBOutlet weak var textFieldScheduleTime: PDTextField!
    @IBOutlet weak var textFieldScheduleDate: PDTextField!
    @IBOutlet weak var textFieldPlanAmount: PDTextField!
    @IBOutlet weak var textFieldPlanCollected: PDTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        DateInputView.addDatePickerForTextField(textFieldScheduleTime, minimumDate: nil, maximumDate: nil, mode: UIDatePickerMode.Time, interval: 30)
        DateInputView.addDatePickerForTextField(textFieldScheduleDate, minimumDate: nil, maximumDate: nil)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonNextAction() {
        if radioPlanReviewed.selectedButton == nil {
            self.showAlert("PLEASE SELECT PLAN REVIEWED OR NOT")
        } else if textFieldAuthSent.isEmpty || textFieldAuthReceived.isEmpty {
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        } else if textFieldPlanAmount.isEmpty || textFieldPlanCollected.isEmpty {
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        } else if textFieldScheduleTime.isEmpty || textFieldScheduleDate.isEmpty {
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        } else {
            let checklistStep2VC = self.storyboard?.instantiateViewControllerWithIdentifier("kCheckListStep2VC") as! CheckListStep2VC
            checklistStep2VC.checkList = checkList
            self.navigationController?.pushViewController(checklistStep2VC, animated: true)
        }
    }
    var checkList: CheckList {
        get {
            let check: CheckList = CheckList()
            check.treatmentPlanReviewed = radioPlanReviewed.selected
            check.xraysDone = radioXrays.selectedButton == nil ? 0 : radioXrays.selectedButton.tag
            check.authSent = textFieldAuthSent.text!
            check.authReceived = textFieldAuthReceived.text!
            check.scheduleTime = textFieldScheduleTime.text!
            check.scheduleDate = textFieldScheduleDate.text!
            check.planAmount = textFieldPlanAmount.text!
            check.planCollection = textFieldPlanCollected.text!
            
            return check
        }
    }
}
extension CheckListStep1VC: UITextFieldDelegate {
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldPlanAmount || textField == textFieldPlanCollected {
            return textField.formatAmount(range, string: string)
        }
        return true
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
class CheckList: NSObject {
    var treatmentPlanReviewed: Bool!
    var xraysDone: Int!
    var authSent: String!
    var authReceived: String!
    var scheduleTime: String!
    var scheduleDate: String!
    var planAmount: String!
    var planCollection: String!
    
    var appointmentConfirmed: Bool!
    var formsCompletedDate: String!
    var completedForms: [Int]!
    var givenDate: String!
    var givenToWhom: String!
    
    var usedRestRoom: Bool!
    var dateCheckListOption: Int!
    var formFilledOutCompletely: Bool!
    var slipStappedToSedationForm: Bool!
}
