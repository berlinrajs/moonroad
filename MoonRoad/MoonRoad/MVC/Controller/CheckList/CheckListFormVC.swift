//
//  CheckListFormVC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 07/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class CheckListFormVC: PDViewController {

    var checkList: CheckList!
    
    
    @IBOutlet weak var buttonPlanReviewed: UIButton!
    @IBOutlet weak var radioXrays: RadioButton!
    @IBOutlet weak var labelAuthSent: UILabel!
    @IBOutlet weak var labelAuthReceived: UILabel!
    @IBOutlet weak var labelScheduleTime: UILabel!
    @IBOutlet weak var labelScheduleDate: UILabel!
    @IBOutlet weak var labelPlanAmount: UILabel!
    @IBOutlet weak var labelPlanCollected: UILabel!
    
    @IBOutlet weak var buttonAppointment: UIButton!
    @IBOutlet weak var labelConsentForms: UILabel!
    @IBOutlet weak var labelGivenDate: UILabel!
    @IBOutlet weak var labelGivenTo: UILabel!
    
    @IBOutlet weak var buttonRestRoom: UIButton!
    @IBOutlet weak var radioOptions: RadioButton!
    @IBOutlet weak var buttonSedation: UIButton!
    @IBOutlet weak var buttonBP: UIButton!
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet var buttonOptions: [UIButton]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for button in buttonOptions {
            button.setTitleColor(UIColor.blackColor(), forState: UIControlState.Selected)
            button.selected = checkList.completedForms.contains(button.tag)
        }
        labelName.text = patient.fullName
        
        buttonPlanReviewed.setTitleColor(UIColor.blackColor(), forState: UIControlState.Selected)
        buttonAppointment.setTitleColor(UIColor.blackColor(), forState: UIControlState.Selected)
        buttonRestRoom.setTitleColor(UIColor.blackColor(), forState: UIControlState.Selected)
        buttonSedation.setTitleColor(UIColor.blackColor(), forState: UIControlState.Selected)
        buttonBP.setTitleColor(UIColor.blackColor(), forState: UIControlState.Selected)
        for button in radioOptions.groupButtons {
            button.setTitleColor(UIColor.blackColor(), forState: UIControlState.Selected)
        }
        
        buttonPlanReviewed.selected = checkList.treatmentPlanReviewed
        radioXrays.setSelectedWithTag(checkList.xraysDone)
        labelAuthSent.text = checkList.authSent
        labelAuthReceived.text = checkList.authReceived
        labelScheduleTime.text = checkList.scheduleTime
        labelScheduleDate.text = checkList.scheduleDate
        labelPlanAmount.text = checkList.planAmount
        labelPlanCollected.text = checkList.planCollection
        buttonAppointment.selected = checkList.appointmentConfirmed
        labelConsentForms.text = checkList.formsCompletedDate
        labelGivenDate.text = checkList.givenDate
        labelGivenTo.text = checkList.givenToWhom
        buttonRestRoom.selected = checkList.usedRestRoom
        radioOptions.setSelectedWithTag(checkList.dateCheckListOption)
        buttonSedation.selected = checkList.formFilledOutCompletely
        buttonBP.selected = checkList.slipStappedToSedationForm
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
