//
//  BracesInstructionFormViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 7/4/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class BracesInstructionFormViewController: PDViewController {

    var patientSignature : UIImage!
    var patientInitial : UIImage!
    var witnessSignature : UIImage!
    
    @IBOutlet weak var patientInitial1 : UIImageView!
    @IBOutlet weak var patientInitial2 : UIImageView!
    @IBOutlet weak var patientSignatureView : UIImageView!
    @IBOutlet weak var witnessSignatureView : UIImageView!
    @IBOutlet weak var labelDate : UILabel!
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDateBirth: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelName.text = patient.fullName
        labelDateBirth.text = patient.dateOfBirth
        
        patientInitial1.image = patientInitial
        patientInitial2.image = patientInitial
        patientSignatureView.image = patientSignature
        witnessSignatureView.image = witnessSignature
        labelDate.text = patient.dateToday


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
