//
//  BracesInstructionViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 7/4/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class BracesInstructionViewController: PDViewController {
    
    @IBOutlet weak var imageviewPatientSignature : SignatureView!
    @IBOutlet weak var imageviewPatientInitial : SignatureView!
    @IBOutlet weak var imageViewWitnessSignature : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (sender : UIButton){
        if  !imageviewPatientSignature.isSigned() || !imageviewPatientInitial.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !imageViewWitnessSignature.isSigned() {
            self.showAlert("PLEASE GET SIGN FROM WITNESS")
        } else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }
        else{
            let new1VC = newStoryBoard.instantiateViewControllerWithIdentifier("BracesInstructionFormVC") as! BracesInstructionFormViewController
            new1VC.patientSignature = imageviewPatientSignature.signatureImage()
            new1VC.witnessSignature = imageViewWitnessSignature.signatureImage()
            new1VC.patientInitial = imageviewPatientInitial.signatureImage()
//            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
            
        }

    }
    


}
