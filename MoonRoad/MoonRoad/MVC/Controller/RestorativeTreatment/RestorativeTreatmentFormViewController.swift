//
//  RestorativeTreatmentFormViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 7/1/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class RestorativeTreatmentFormViewController: PDViewController {
    
    @IBOutlet weak var imageViewPatient : UIImageView!
    @IBOutlet weak var imageViewWitness : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var labelToothNumber : UILabel!
    @IBOutlet weak var labelShade : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        imageViewPatient.image = patient.restorativePatientSign
        imageViewWitness.image = patient.restorativeWitnessSign
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        labelToothNumber.text = patient.restorativeToothNumber
        labelShade.text = patient.restorativeShade
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
