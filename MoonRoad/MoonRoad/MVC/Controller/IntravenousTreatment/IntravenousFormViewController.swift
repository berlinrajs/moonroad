//
//  IntravenousFormViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 7/1/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class IntravenousFormViewController: PDViewController {

    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var signatureWitness : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        signaturePatient.image = patient.intravenousSignaturePatient
        signatureWitness.image = patient.intravenousSignatureWitness
        labelDate1.text = patient.dateToday

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
