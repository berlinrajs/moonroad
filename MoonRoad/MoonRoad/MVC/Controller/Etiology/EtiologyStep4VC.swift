//
//  EtiologyStep4VC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 08/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class EtiologyStep4VC: PDViewController {

    var etiology: Etiology!
    @IBOutlet weak var buttonVerified: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        etiology.questionGroup4 = [Int]()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonGroupAction(sender: UIButton) {
        sender.selected = !sender.selected
        if sender.selected {
            if !etiology.questionGroup4.contains(sender.tag) {
                etiology.questionGroup4.append(sender.tag)
            }
        } else {
            if etiology.questionGroup4.contains(sender.tag) {
                etiology.questionGroup4.removeAtIndex(etiology.questionGroup4.indexOf(sender.tag)!)
            }
        }
    }
    @IBAction func buttonVerifiedAction(sender: UIButton) {
        sender.selected = !sender.selected
    }
    
    @IBAction func buttonNextAction() {
        if !buttonVerified.selected {
            self.showAlert("PEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
        } else {
            let step5 = self.storyboard?.instantiateViewControllerWithIdentifier("kEtiologyStep5VC") as! EtiologyStep5VC
            step5.etiology = self.etiology
            self.navigationController?.pushViewController(step5, animated: true)
        }
    }
}
