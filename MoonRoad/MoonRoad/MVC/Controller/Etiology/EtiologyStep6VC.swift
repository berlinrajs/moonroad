//
//  EtiologyStep6VC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 08/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class EtiologyStep6VC: PDViewController {

    var etiology: Etiology!
    @IBOutlet weak var buttonVerified: UIButton!
    @IBOutlet weak var textFieldCanals: PDTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        etiology.questionGroup6 = [Int]()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonGroupAction(sender: UIButton) {
        sender.selected = !sender.selected
        if sender.selected {
            if !etiology.questionGroup6.contains(sender.tag) {
                etiology.questionGroup6.append(sender.tag)
            }
        } else {
            if etiology.questionGroup6.contains(sender.tag) {
                etiology.questionGroup6.removeAtIndex(etiology.questionGroup6.indexOf(sender.tag)!)
            }
        }
        if sender.tag == 2 {
            if sender.selected {
                PopupTextField.popUpView().showWithTitle("PLEASE SPECIFY THE ROOT CANAL", placeHolder: "PLEASE SPECIFY", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default, inViewController: self, completion: { (popUpView, textField, isEdited) in
                    if textField.isEmpty {
                        self.etiology.partialyClasified = nil
                        sender.selected = false
                        if self.etiology.questionGroup6.contains(sender.tag) {
                            self.etiology.questionGroup6.removeAtIndex(self.etiology.questionGroup6.indexOf(sender.tag)!)
                        }
                    } else {
                        self.etiology.partialyClasified = textField.text!
                    }
                })
            } else {
                self.etiology.partialyClasified = nil
                sender.selected = false
                if self.etiology.questionGroup6.contains(sender.tag) {
                    self.etiology.questionGroup6.removeAtIndex(self.etiology.questionGroup6.indexOf(sender.tag)!)
                }
            }
        } else if sender.tag == 3 {
            if sender.selected {
                PopupTextField.popUpView().showWithTitle("PLEASE SPECIFY THE ROOT CANAL", placeHolder: "PLEASE SPECIFY", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default, inViewController: self, completion: { (popUpView, textField, isEdited) in
                    if textField.isEmpty {
                        self.etiology.completelyClasified = nil
                        sender.selected = false
                        if self.etiology.questionGroup6.contains(sender.tag) {
                            self.etiology.questionGroup6.removeAtIndex(self.etiology.questionGroup6.indexOf(sender.tag)!)
                        }
                    } else {
                        self.etiology.completelyClasified = textField.text!
                    }
                })
            } else {
                self.etiology.completelyClasified = nil
                sender.selected = false
                if self.etiology.questionGroup6.contains(sender.tag) {
                    self.etiology.questionGroup6.removeAtIndex(self.etiology.questionGroup6.indexOf(sender.tag)!)
                }
            }
        }
    }
    @IBAction func buttonVerifiedAction(sender: UIButton) {
        sender.selected = !sender.selected
    }
    
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        if !buttonVerified.selected {
            self.showAlert("PEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
        } else {
            etiology.numberOfCanal = textFieldCanals.isEmpty ? "" : textFieldCanals.text
            let step7 = self.storyboard?.instantiateViewControllerWithIdentifier("kEtiologyStep7VC") as! EtiologyStep7VC
            step7.etiology = self.etiology
            self.navigationController?.pushViewController(step7, animated: true)
        }
    }
}
extension EtiologyStep6VC: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        return textField.formatNumbers(range, string: string, count: 5)
    }
}
