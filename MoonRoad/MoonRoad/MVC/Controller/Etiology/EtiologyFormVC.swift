//
//  EtiologyFormVC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 08/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class EtiologyFormVC: PDViewController {

    var etiology: Etiology!
    
    @IBOutlet var buttonOptions1: [UIButton]!
    @IBOutlet var buttonOptions2: [UIButton]!
    @IBOutlet var buttonOptions3: [UIButton]!
    @IBOutlet var buttonOptions4: [UIButton]!
    @IBOutlet var buttonOptions5: [UIButton]!
    @IBOutlet var buttonOptions6: [UIButton]!
    @IBOutlet var buttonOptions7: [UIButton]!
    @IBOutlet var textFieldsOptions8: [UILabel]!
    
    @IBOutlet weak var labelOther: UILabel!
    @IBOutlet weak var labelPrimary: UILabel!
    @IBOutlet weak var labelComplete: UILabel!
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelToothNumbers: UILabel!
    
    @IBOutlet weak var numberOfCanals: UILabel!
    @IBOutlet weak var numberOfLA: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        for button in buttonOptions1 {
            button.selected = etiology.questionGroup1.contains(button.tag)
        }
        for button in buttonOptions2 {
            button.selected = etiology.questionGroup2.contains(button.tag)
        }
        for button in buttonOptions3 {
            button.selected = etiology.questionGroup3.contains(button.tag)
        }
        for button in buttonOptions4 {
            button.selected = etiology.questionGroup4.contains(button.tag)
        }
        for button in buttonOptions5 {
            button.selected = etiology.questionGroup5.contains(button.tag)
        }
        for button in buttonOptions6 {
            button.selected = etiology.questionGroup6.contains(button.tag)
        }
        for button in buttonOptions7 {
            button.selected = etiology.questionGroup7.contains(button.tag)
        }
        for label in textFieldsOptions8 {
            label.text = etiology.questionGroup8[label.tag - 1]
        }
        
        labelOther.text = etiology.otherCause
        labelPrimary.text = etiology.partialyClasified
        labelComplete.text = etiology.completelyClasified
        numberOfCanals.text = etiology.numberOfCanal
        numberOfLA.text = etiology.numberOfLA
        
        labelName.text = patient.fullName
        labelDate.text = patient.dateToday
        labelToothNumbers.text = patient.selectedForms.first!.toothNumbers
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
