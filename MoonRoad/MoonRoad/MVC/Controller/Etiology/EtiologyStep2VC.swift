//
//  EtiologyStep2VC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 08/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class EtiologyStep2VC: PDViewController {

    var etiology: Etiology!
    @IBOutlet weak var buttonVerified: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        etiology.questionGroup2 = [Int]()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonGroupAction(sender: UIButton) {
        sender.selected = !sender.selected
        if sender.selected {
            if !etiology.questionGroup2.contains(sender.tag) {
                etiology.questionGroup2.append(sender.tag)
            }
        } else {
            if etiology.questionGroup2.contains(sender.tag) {
                etiology.questionGroup2.removeAtIndex(etiology.questionGroup2.indexOf(sender.tag)!)
            }
        }
    }
    @IBAction func buttonVerifiedAction(sender: UIButton) {
        sender.selected = !sender.selected
    }
    
    @IBAction func buttonNextAction() {
        if !buttonVerified.selected {
            self.showAlert("PEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
        } else {
            let step3 = self.storyboard?.instantiateViewControllerWithIdentifier("kEtiologyStep3VC") as! EtiologyStep3VC
            step3.etiology = self.etiology
            self.navigationController?.pushViewController(step3, animated: true)
        }
    }
}
