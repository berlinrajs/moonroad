//
//  EtiologyStep7VC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 08/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class EtiologyStep7VC: PDViewController {

    var etiology: Etiology!
    @IBOutlet weak var buttonVerified: UIButton!
    @IBOutlet weak var textFieldLA: PDTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        etiology.questionGroup7 = [Int]()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonGroupAction(sender: UIButton) {
        sender.selected = !sender.selected
        if sender.selected {
            if !etiology.questionGroup7.contains(sender.tag) {
                etiology.questionGroup7.append(sender.tag)
            }
        } else {
            if etiology.questionGroup7.contains(sender.tag) {
                etiology.questionGroup7.removeAtIndex(etiology.questionGroup7.indexOf(sender.tag)!)
            }
        }
    }
    @IBAction func buttonVerifiedAction(sender: UIButton) {
        sender.selected = !sender.selected
    }
    
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        if !buttonVerified.selected {
            self.showAlert("PEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
        } else {
            etiology.numberOfLA = textFieldLA.isEmpty ? "" : textFieldLA.text
            let step8 = self.storyboard?.instantiateViewControllerWithIdentifier("kEtiologyStep8VC") as! EtiologyStep8VC
            step8.etiology = self.etiology
            self.navigationController?.pushViewController(step8, animated: true)
        }
    }
}
extension EtiologyStep7VC: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        return textField.formatNumbers(range, string: string, count: 5)
    }
}
