//
//  EtiologyStep8VC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 08/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class EtiologyStep8VC: PDViewController {

    var etiology: Etiology!
    @IBOutlet weak var buttonVerified: UIButton!
    
    @IBOutlet weak var textFieldSize1: PDTextField!
    @IBOutlet weak var textFieldSize2: PDTextField!
    @IBOutlet weak var textFieldSize3: PDTextField!
    @IBOutlet weak var textFieldSize4: PDTextField!
    @IBOutlet weak var textFieldSize5: PDTextField!
    @IBOutlet weak var textFieldSize6: PDTextField!
    @IBOutlet weak var textFieldSize7: PDTextField!
    @IBOutlet weak var textFieldSize8: PDTextField!
    @IBOutlet weak var textFieldSize9: PDTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
//    @IBAction func buttonGroupAction(sender: UIButton) {
    
//        sender.selected = !sender.selected
//        if sender.selected {
//            if !etiology.questionGroup8.contains(sender.tag) {
//                etiology.questionGroup8.append(sender.tag)
//            }
//        } else {
//            if etiology.questionGroup8.contains(sender.tag) {
//                etiology.questionGroup8.removeAtIndex(etiology.questionGroup8.indexOf(sender.tag)!)
//            }
//        }
//    }
    @IBAction func buttonVerifiedAction(sender: UIButton) {
        sender.selected = !sender.selected
    }
    
    @IBAction func buttonNextAction() {
        if !buttonVerified.selected {
            self.showAlert("PEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
        } else {
            
            etiology.questionGroup8 = [textFieldSize1.text!,
                textFieldSize2.text!,
                textFieldSize3.text!,
                textFieldSize4.text!,
                textFieldSize5.text!,
                textFieldSize6.text!,
                textFieldSize7.text!,
                textFieldSize8.text!,
                textFieldSize9.text!]
            
            let formvc = self.storyboard?.instantiateViewControllerWithIdentifier("kEtiologyFormVC") as! EtiologyFormVC
            formvc.etiology = self.etiology
            self.navigationController?.pushViewController(formvc, animated: true)
        }
    }
}
extension EtiologyStep8VC: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        return textField.formatNumbers(range, string: string, count: 2)
    }
}
