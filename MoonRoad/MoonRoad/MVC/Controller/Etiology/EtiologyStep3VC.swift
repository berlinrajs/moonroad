//
//  EtiologyStep3VC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 08/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class EtiologyStep3VC: PDViewController {

    var etiology: Etiology!
    @IBOutlet weak var buttonVerified: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        etiology.questionGroup3 = [Int]()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonGroupAction(sender: UIButton) {
        sender.selected = !sender.selected
        if sender.selected {
            if !etiology.questionGroup3.contains(sender.tag) {
                etiology.questionGroup3.append(sender.tag)
            }
        } else {
            if etiology.questionGroup3.contains(sender.tag) {
                etiology.questionGroup3.removeAtIndex(etiology.questionGroup3.indexOf(sender.tag)!)
            }
        }
    }
    @IBAction func buttonVerifiedAction(sender: UIButton) {
        sender.selected = !sender.selected
    }
    
    @IBAction func buttonNextAction() {
        if !buttonVerified.selected {
            self.showAlert("PEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
        } else {
            let step4 = self.storyboard?.instantiateViewControllerWithIdentifier("kEtiologyStep4VC") as! EtiologyStep4VC
            step4.etiology = self.etiology
            self.navigationController?.pushViewController(step4, animated: true)
        }
    }
}
