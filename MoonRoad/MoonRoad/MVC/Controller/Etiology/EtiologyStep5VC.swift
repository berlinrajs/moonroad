//
//  EtiologyStep5VC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 08/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class EtiologyStep5VC: PDViewController {

    var etiology: Etiology!
    @IBOutlet weak var buttonVerified: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        etiology.questionGroup5 = [Int]()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonGroupAction(sender: UIButton) {
        sender.selected = !sender.selected
        if sender.selected {
            if !etiology.questionGroup5.contains(sender.tag) {
                etiology.questionGroup5.append(sender.tag)
            }
        } else {
            if etiology.questionGroup5.contains(sender.tag) {
                etiology.questionGroup5.removeAtIndex(etiology.questionGroup5.indexOf(sender.tag)!)
            }
        }
    }
    @IBAction func buttonVerifiedAction(sender: UIButton) {
        sender.selected = !sender.selected
    }
    
    @IBAction func buttonNextAction() {
        if !buttonVerified.selected {
            self.showAlert("PEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
        } else {
            let step6 = self.storyboard?.instantiateViewControllerWithIdentifier("kEtiologyStep6VC") as! EtiologyStep6VC
            step6.etiology = self.etiology
            self.navigationController?.pushViewController(step6, animated: true)
        }
    }
}
