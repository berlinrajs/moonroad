//
//  EtiologyStep1VC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 08/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class EtiologyStep1VC: PDViewController {

    var etiology: Etiology!
    @IBOutlet weak var buttonVerified: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        etiology = Etiology()
        etiology.questionGroup1 = [Int]()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonGroupAction(sender: UIButton) {
        sender.selected = !sender.selected
        if sender.selected {
            if !etiology.questionGroup1.contains(sender.tag) {
                etiology.questionGroup1.append(sender.tag)
            }
        } else {
            if etiology.questionGroup1.contains(sender.tag) {
               etiology.questionGroup1.removeAtIndex(etiology.questionGroup1.indexOf(sender.tag)!)
            }
        }
        if sender.tag == 12 {
            if sender.selected {
                PopupTextField.popUpView().showWithTitle("OTHER CAUSE", placeHolder: "PLEASE SPECIFY", keyboardType: UIKeyboardType.Default, textFormat: TextFormat.Default, inViewController: self, completion: { (popUpView, textField, isEdited) in
                    if textField.isEmpty {
                        self.etiology.otherCause = nil
                        sender.selected = false
                        if self.etiology.questionGroup1.contains(sender.tag) {
                            self.etiology.questionGroup1.removeAtIndex(self.etiology.questionGroup1.indexOf(sender.tag)!)
                        }
                    } else {
                        self.etiology.otherCause = textField.text!
                    }
                })
            } else {
                self.etiology.otherCause = nil
                sender.selected = false
                if self.etiology.questionGroup1.contains(sender.tag) {
                    self.etiology.questionGroup1.removeAtIndex(self.etiology.questionGroup1.indexOf(sender.tag)!)
                }
            }
        }
    }
    @IBAction func buttonVerifiedAction(sender: UIButton) {
        sender.selected = !sender.selected
    }
    
    @IBAction func buttonNextAction() {
        if !buttonVerified.selected {
            self.showAlert("PEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
        } else {
            let step2 = self.storyboard?.instantiateViewControllerWithIdentifier("kEtiologyStep2VC") as! EtiologyStep2VC
            step2.etiology = self.etiology
            self.navigationController?.pushViewController(step2, animated: true)
        }
    }
}

class Etiology: NSObject {
    var questionGroup1: [Int]!
    var questionGroup2: [Int]!
    var questionGroup3: [Int]!
    var questionGroup4: [Int]!
    var questionGroup5: [Int]!
    var questionGroup6: [Int]!
    var questionGroup7: [Int]!
    var questionGroup8: [String]!
    
    var otherCause: String?
    var partialyClasified: String?
    var completelyClasified: String?
    
    var numberOfCanal: String!
    var numberOfLA: String!
}
