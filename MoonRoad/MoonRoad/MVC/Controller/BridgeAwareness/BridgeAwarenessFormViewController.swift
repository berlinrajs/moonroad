//
//  BridgeAwarenessFormViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 6/30/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class BridgeAwarenessFormViewController: PDViewController {

    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var signatureWitness : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelName: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelName.text = patient.fullName
        signaturePatient.image = patient.bridgeSignaturePatient
        signatureWitness.image = patient.bridgeSignatureDoctor
        labelDate1.text = patient.dateToday

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
