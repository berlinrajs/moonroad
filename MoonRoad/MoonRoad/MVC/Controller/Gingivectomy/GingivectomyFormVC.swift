//
//  GingivectomyFormVC.swift
//  MoonRoad
//
//  Created by Berlin Raj on 23/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class GingivectomyFormVC: PDViewController {

    var signedName: String!
    var signature: UIImage!
    var signedRelation: String!
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var signatureView: UIImageView!
    @IBOutlet weak var labelPatientName: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelName2: UILabel!
    @IBOutlet weak var labelRelation: UILabel!
    @IBOutlet weak var labelDOB: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        labelDate.text = patient.dateToday
        signatureView.image = signature
        labelPatientName.text = patient.fullName
        labelName.text = signedName
        labelName2.text = signedName != patient.fullName ? signedName : ""
        labelRelation.text = signedRelation
        labelDOB.text = patient.dateOfBirth
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
