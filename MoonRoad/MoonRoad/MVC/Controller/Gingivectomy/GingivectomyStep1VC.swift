//
//  GingivectomyStep1VC.swift
//  MoonRoad
//
//  Created by Berlin Raj on 23/11/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class GingivectomyStep1VC: PDViewController {

    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var textFieldName: PDTextField!
    @IBOutlet weak var textFieldRelation: PDTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        labelName.text = patient.fullName
        // Do any additional setup after loading the view.
    }

    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        if !signatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE TAP THE DATE")
        } else {
            let formVC = self.storyboard?.instantiateViewControllerWithIdentifier("kGingivectomyFormVC") as! GingivectomyFormVC
            formVC.signature = signatureView.signatureImage()
            formVC.signedName = labelName.text!
            formVC.signedRelation = textFieldRelation.text!
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension GingivectomyStep1VC: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldName {
            let newRange = textField.text!.startIndex.advancedBy(range.location)..<textField.text!.startIndex.advancedBy(range.location + range.length)
            let newName = textField.text!.stringByReplacingCharactersInRange(newRange, withString: string)
            
            labelName.text = newName.characters.count > 0 || !textFieldRelation.isEmpty ? newName : patient.fullName
        } else {
            let newRange = textField.text!.startIndex.advancedBy(range.location)..<textField.text!.startIndex.advancedBy(range.location + range.length)
            let newRelation = textField.text!.stringByReplacingCharactersInRange(newRange, withString: string)
            
            labelName.text = newRelation.characters.count > 0 || !textFieldName.isEmpty ? textFieldName.text! : patient.fullName
        }
        
        return true
    }
}
