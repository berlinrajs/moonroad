//
//  CompleteVisitFormViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 7/7/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class CompleteVisitFormViewController: PDViewController {

    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var labelName : UILabel!
    @IBOutlet weak var labelDOB : UILabel!
    @IBOutlet weak var labelSSN : UILabel!
    @IBOutlet weak var labelEmail : UILabel!
    @IBOutlet weak var labelCellNumber : UILabel!
    @IBOutlet var arrayAddressLabels : [UILabel]!
    @IBOutlet weak var radioNewAddress : RadioButton!
    @IBOutlet weak var labelPhoneNumber : UILabel!
    @IBOutlet weak var labelAppointment : UILabel!
    @IBOutlet weak var labelReason : UILabel!
    @IBOutlet weak var labelInsurance : UILabel!
    @IBOutlet weak var radioNewInsurance : RadioButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate.text = patient.dateToday
        labelName.text = patient.fullName
        labelDOB.text = patient.dateOfBirth
        labelSSN.text = patient.visit.socialSecurityNumber
        labelEmail.text = patient.visit.emailAddress
        labelCellNumber.text = patient.visit.cellNumber
        patient.visit.address.setTextForArrayOfLabels(arrayAddressLabels)
        radioNewAddress.setSelectedWithTag(patient.visit.addressTag)
        labelPhoneNumber.text = patient.visit.phoneNumber
        labelAppointment.text = patient.visit.timeOfAppointment
        labelReason.text = patient.visit.reason
        labelInsurance.text = patient.visit.insurance
        radioNewInsurance.setSelectedWithTag(patient.visit.insuranceTag)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
