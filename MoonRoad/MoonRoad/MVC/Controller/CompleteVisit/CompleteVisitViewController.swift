//
//  CompleteVisitViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 7/5/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class CompleteVisitViewController: PDViewController {
    
    @IBOutlet weak var textfieldSSN : UITextField!
    @IBOutlet weak var textfieldEmail : UITextField!
    @IBOutlet weak var textfieldCellNumber : UITextField!
    @IBOutlet weak var textfieldPhoneNumber : UITextField!
    @IBOutlet weak var textfieldAddress : UITextField!
    @IBOutlet weak var radioButtonAddress : RadioButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        radioButtonAddress.setSelectedWithTag(2)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onNextButtonPressed (sender : UIButton){
        if textfieldSSN.isEmpty || textfieldCellNumber.isEmpty || textfieldAddress.isEmpty{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")

        }else if !textfieldSSN.text!.isSocialSecurityNumber{
            self.showAlert("PLEASE ENTER THE VALID SOCIAL SECURITY NUMBER")

        }else if !textfieldEmail.isEmpty && !textfieldEmail.text!.isValidEmail{
            self.showAlert("PLEASE ENTER THE VALID EMAIL")
        }else if !textfieldCellNumber.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID CELL NUMBER")

        }else if (!textfieldPhoneNumber.isEmpty && !textfieldPhoneNumber.text!.isPhoneNumber){
            self.showAlert("PLEASE ENTER THE VALID PHONE NUMBER")
        }else{
            patient.visit.socialSecurityNumber = textfieldSSN.text!
            patient.visit.emailAddress = textfieldEmail.isEmpty ? "N/A" : textfieldEmail.text!
            patient.visit.cellNumber = textfieldCellNumber.text!
            patient.visit.phoneNumber = textfieldPhoneNumber.isEmpty ? "N/A" : textfieldPhoneNumber.text!
            patient.visit.address = textfieldAddress.text!
            patient.visit.addressTag = radioButtonAddress.selectedButton.tag
            let new1VC = newStoryBoard.instantiateViewControllerWithIdentifier("CompleteVisit1VC") as! CompleteVisit1ViewController
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
        
    }
}

extension CompleteVisitViewController : UITextFieldDelegate{
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldSSN {
            return textField.formatSocialSecurityNumber(range, string: string)
        } else if textField == textfieldCellNumber || textField == textfieldPhoneNumber {
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
