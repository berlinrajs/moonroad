//
//  CompleteVisit1ViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 7/7/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class CompleteVisit1ViewController: PDViewController {

    @IBOutlet weak var textfieldAppointment : UITextField!
    @IBOutlet weak var textfieldReason : UITextField!
    @IBOutlet weak var textfieldInsurance : UITextField!
    @IBOutlet weak var radioInsurance : RadioButton!
    @IBOutlet weak var viewInsurance: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        DateInputView.addTimePickerForTextField(textfieldAppointment)
//        radioInsurance.setSelectedWithTag(2)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    @IBAction func onNextButtonPressed (sender : UIButton){
        self.view.endEditing(true)
        if textfieldAppointment.isEmpty || textfieldReason.isEmpty{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        } else if !textfieldInsurance.isEmpty && radioInsurance.selectedButton == nil {
            self.showAlert("PLEASE SPECIFY IS THIS NEW INSURANCE OR NOT")
        } else {
            patient.visit.timeOfAppointment = textfieldAppointment.text!
            patient.visit.reason = textfieldReason.text!
            patient.visit.insurance = textfieldInsurance.isEmpty ? "N/A" : textfieldInsurance.text!
            patient.visit.insuranceTag = textfieldInsurance.isEmpty ? 0 : radioInsurance.selectedButton.tag
            
            let new1VC = newStoryBoard.instantiateViewControllerWithIdentifier("CompleteVisitFormVC") as! CompleteVisitFormViewController
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
    }
}

extension CompleteVisit1ViewController : UITextFieldDelegate{
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldReason {
            let newRange = textField.text!.startIndex.advancedBy(range.location)..<textField.text!.startIndex.advancedBy(range.location + range.length)
            let newString = textField.text!.stringByReplacingCharactersInRange(newRange, withString: string)
            if newString.characters.count > 50 {
                return false
            }
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == textfieldInsurance {
            viewInsurance.hidden = textField.isEmpty
            radioInsurance.deselectAllButtons()
        }
    }
}

