//
//  RootCanal1ViewController.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 01/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class RootCanal1ViewController: PDViewController {
    
    @IBOutlet weak var labelDetails: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        patient.rootCanalSelectedComplications = [Int]()
        labelDetails.text = labelDetails.text!.stringByReplacingOccurrencesOfString("PATIENT_TOOTH_CONDITION", withString: patient.selectedForms.first!.additionalDetails)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonComplicationAction(sender: UIButton) {
        if sender.selected == false {
            sender.selected = true
            if !patient.rootCanalSelectedComplications.contains(sender.tag) {
                patient.rootCanalSelectedComplications.append(sender.tag)
            }
        } else {
            sender.selected = false
            if patient.rootCanalSelectedComplications.contains(sender.tag) {
                patient.rootCanalSelectedComplications.removeAtIndex(patient.rootCanalSelectedComplications.indexOf(sender.tag)!)
            }
        }
    }
    
    @IBAction func buttonNextAction() {
        let step2 = self.storyboard?.instantiateViewControllerWithIdentifier("kRootCanal2ViewController") as! RootCanal2ViewController
//        step2.patient = self.patient
        self.navigationController?.pushViewController(step2, animated: true)
    }
}
