//
//  RootCanal2ViewController.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 01/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class RootCanal2ViewController: PDViewController {

    @IBOutlet weak var patientSignatureView: SignatureView!
    @IBOutlet weak var witnessSignature: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func buttonNextAction() {
        if !patientSignatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !witnessSignature.isSigned() {
            self.showAlert("PLEASE GET SIGN FROM WITNESS")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        } else {
            patient.rootCanalWitnessSign = witnessSignature.signatureImage()
            patient.rootCanalSignture = patientSignatureView.signatureImage()
            
            let form = self.storyboard?.instantiateViewControllerWithIdentifier("kRootCanalFormViewController") as! RootCanalFormViewController
//            form.patient = self.patient
            self.navigationController?.pushViewController(form, animated: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
