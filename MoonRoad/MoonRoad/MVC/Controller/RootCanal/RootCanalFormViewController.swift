//
//  RootCanalFormViewController.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 01/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class RootCanalFormViewController: PDViewController {

    @IBOutlet weak var patientSignatureView: UIImageView!
    @IBOutlet weak var witnessSignature: UIImageView!
    @IBOutlet weak var labelDate: UILabel!
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelTeethNumber: UILabel!
    @IBOutlet weak var labelCondition: UILabel!
    
    @IBOutlet var buttonOptions: [UIButton]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        patientSignatureView.image = patient.rootCanalSignture
        witnessSignature.image = patient.rootCanalWitnessSign
        labelDate.text = patient.dateToday
        labelName.text = patient.fullName
        labelTeethNumber.text = patient.selectedForms.first?.toothNumbers
        labelCondition.text = patient.selectedForms.first?.additionalDetails
        
        for button in buttonOptions {
            button.selected = patient.rootCanalSelectedComplications.contains(button.tag)
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
