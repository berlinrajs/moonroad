//
//  LoginViewController.swift
//  OptimaDentistry
//
//  Created by SRS Web Solutions on 12/07/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class LoginViewController: PDViewController {

    @IBOutlet weak var textFieldUserName: PDTextField!
    @IBOutlet weak var textFieldPassword: PDTextField!
    @IBOutlet weak var labelVersion: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(dateChangedNotification), name: kDateChangedNotification, object: nil)
        self.dateChangedNotification()
        labelVersion.text = NSBundle.mainBundle().objectForInfoDictionaryKey(kCFBundleVersionKey as String) as? String
        // Do any additional setup after loading the view.
    }
    
    func dateChangedNotification() {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        labelDate.text = dateFormatter.stringFromDate(NSDate()).uppercaseString
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonSubmitAction() {
        if textFieldUserName.isEmpty || !textFieldUserName.text!.isValidEmail {
            self.showAlert("PLEASE ENTER THE VALID EMAIL")
        } else if textFieldPassword.isEmpty {
            self.showAlert("PLEASE ENTER THE PASSWORD")
        } else {
            self.submitAction()
        }
    }
    
    func submitAction() {
        self.view.endEditing(true)
        if Reachability.isConnectedToNetwork() {
            ServiceManager.loginWithUsername(textFieldUserName.text!, password: textFieldPassword.text!) { (success, error) -> (Void) in
                if success {
                    NSUserDefaults.standardUserDefaults().setBool(true, forKey: kAppLoggedInKey)
                    NSUserDefaults.standardUserDefaults().setValue(self.textFieldUserName.text!, forKey: kAppLoginUsernameKey)
                    NSUserDefaults.standardUserDefaults().setValue(self.textFieldPassword.text!, forKey: kAppLoginPasswordKey)
                    NSUserDefaults.standardUserDefaults().synchronize()
                    (UIApplication.sharedApplication().delegate as! AppDelegate).checkAutologin()
                } else {
                    if error == nil {
                        self.showAlert("PLEASE CHECK YOUR INTERNET CONNECTION AND TRY AGAIN")
                    } else {
                        self.showAlert(error!.localizedDescription.uppercaseString)
                    }
                }
            }
        } else {
            let alertController = UIAlertController(title: "MOON ROAD DENTISTRY", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.Alert)
            let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
            let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                
            }
            alertController.addAction(alertOkAction)
            alertController.addAction(alertCancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == textFieldUserName {
            textFieldPassword.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            self.submitAction()
        }
        return true
    }
}
