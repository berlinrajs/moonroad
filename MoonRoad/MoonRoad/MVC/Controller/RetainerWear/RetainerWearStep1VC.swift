//
//  RetainerWearStep1VC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 04/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class RetainerWearStep1VC: PDViewController {

    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate.todayDate = patient.dateToday
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonNextAction() {
        if !signatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        } else {
            patient.orthodonticSignature = signatureView.signatureImage()
            
            let formVC = self.storyboard?.instantiateViewControllerWithIdentifier("kRetainerWearFormVC") as! RetainerWearFormVC
//            formVC.patient = self.patient
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
}
