//
//  RetainerWearFormVC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 04/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class RetainerWearFormVC: PDViewController {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelToday: UILabel!
    @IBOutlet weak var signatureView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelName.text = patient.fullName
        labelToday.text = patient.dateToday
        signatureView.image = patient.orthodonticSignature
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
