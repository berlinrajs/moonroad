//
//  ProsthondonticTreatmentStep2VC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 05/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ProsthondonticTreatmentStep2VC: PDViewController {

    @IBOutlet weak var patientSignature: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var textFieldShade: PDTextField!
    @IBOutlet weak var textFieldType: PDTextField!
    @IBOutlet weak var signatureWitness: SignatureView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate.todayDate = patient.dateToday
        textFieldShade.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonNextAction() {
        if !patientSignature.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        } else if !signatureWitness.isSigned() {
            self.showAlert("PLEASE GET SIGN FROM WITNESS")
        } else {
            patient.prosthondonticPatientSignature = patientSignature.signatureImage()
            patient.prosthondonticShade = textFieldShade.isEmpty ? "" : textFieldShade.text!
            patient.prosthondonticType = textFieldType.isEmpty ? "" : textFieldType.text!
            patient.prosthondonticWitness = signatureWitness.signatureImage()
            
            let formVC = self.storyboard?.instantiateViewControllerWithIdentifier("kProsthondonticTreatmentFormVC") as! ProsthondonticTreatmentFormVC
//            formVC.patient = self.patient
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
}
extension ProsthondonticTreatmentStep2VC: UITextFieldDelegate {
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldShade {
            return textField.formatShade(range, string: string)
        }
        return true
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
