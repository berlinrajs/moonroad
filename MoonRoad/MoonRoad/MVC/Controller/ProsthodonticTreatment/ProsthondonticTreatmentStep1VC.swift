//
//  ProsthondonticTreatmentStep1VC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 05/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ProsthondonticTreatmentStep1VC: PDViewController {

    @IBOutlet var initialViews: [SignatureView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var foundNil: Bool {
        get {
            for initial in initialViews {
                if !initial.isSigned() {
                    return true
                }
            }
            return false
        }
    }
    
    @IBAction func buttonNextAction() {
        if foundNil {
            self.showAlert("PLEASE SIGN THE MISSING INITIALS")
        } else {
            patient.prosthondonticInitials = NSMutableDictionary()
            for initial in initialViews {
                patient.prosthondonticInitials.setValue(initial.signatureImage(), forKey: "\(initial.tag)")
            }
            
            let step2VC = self.storyboard?.instantiateViewControllerWithIdentifier("kProsthondonticTreatmentStep2VC") as! ProsthondonticTreatmentStep2VC
            //        step2VC.patient = self.patient
            self.navigationController?.pushViewController(step2VC, animated: true)
        }
    }
}
