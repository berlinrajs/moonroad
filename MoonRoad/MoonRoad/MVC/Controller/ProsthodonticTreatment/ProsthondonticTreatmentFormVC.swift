//
//  ProsthondonticTreatmentFormVC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 05/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ProsthondonticTreatmentFormVC: PDViewController {

    @IBOutlet weak var patientSignature: UIImageView!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelShade: UILabel!
    @IBOutlet weak var labelType: UILabel!
    @IBOutlet weak var signatureWitness: UIImageView!
    
    @IBOutlet var initialViews: [UIImageView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for initial in initialViews {
            initial.image = patient.prosthondonticInitials["\(initial.tag)"] as? UIImage
        }
        
        labelName.text = patient.fullName
        patientSignature.image = patient.prosthondonticPatientSignature
        labelDate.text = patient.dateToday
        labelShade.text = patient.prosthondonticShade
        labelType.text = patient.prosthondonticType
        signatureWitness.image = patient.prosthondonticWitness

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
