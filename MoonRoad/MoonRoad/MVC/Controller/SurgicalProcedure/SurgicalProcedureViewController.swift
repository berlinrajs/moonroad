//
//  SurgicalProcedureViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 7/5/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SurgicalProcedureViewController: PDViewController {
    
    @IBOutlet weak var textviewCondition : UITextView!
    @IBOutlet weak var signatureViewPatient : SignatureView!
    @IBOutlet weak var signatureViewWitness : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        textviewCondition.text = textviewCondition.text!.stringByReplacingOccurrencesOfString("kCondition", withString: patient.selectedForms.first!.additionalDetails)
        textviewCondition.textColor = UIColor.whiteColor()
        textviewCondition.font = UIFont(name: "Helvetica Neue", size: 15.0)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (sender : UIButton){
        if !signatureViewPatient.isSigned() || !signatureViewWitness.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }
        else{
            let new1VC = newStoryBoard.instantiateViewControllerWithIdentifier("SurgicalFormVC") as! SurgicalProcedureFormViewController
            new1VC.patientSignature = signatureViewPatient.signatureImage()
            new1VC.witnessSignature = signatureViewWitness.signatureImage()
//            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)

        }

    }

}
