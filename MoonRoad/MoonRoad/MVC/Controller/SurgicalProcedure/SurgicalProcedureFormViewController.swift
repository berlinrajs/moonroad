//
//  SurgicalProcedureFormViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 7/5/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SurgicalProcedureFormViewController: PDViewController {

    var patientSignature : UIImage!
    var witnessSignature : UIImage!
    @IBOutlet weak var labelCondition : UILabel!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var labelTooth : UILabel!
    @IBOutlet weak var patientSignatureView : UIImageView!
    @IBOutlet weak var witnessSignatureView : UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelCondition.text = patient.selectedForms.first!.additionalDetails
        labelPatientName.text = patient.fullName
        labelDate.text = patient.dateToday
        labelTooth.text = patient.selectedForms.first!.toothNumbers
        patientSignatureView.image = patientSignature
        witnessSignatureView.image = witnessSignature
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
