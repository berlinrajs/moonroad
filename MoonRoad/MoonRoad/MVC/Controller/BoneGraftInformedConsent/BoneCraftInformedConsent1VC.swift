//
//  BoneGraftInformedConsent1VC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 01/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class BoneGraftInformedConsent1VC: PDViewController {

    @IBOutlet weak var buttonIsSelf: UIButton!
    @IBOutlet weak var buttonIsDonorHuman: UIButton!
    @IBOutlet weak var buttonIsSynthetic: UIButton!
    
    @IBOutlet weak var labelDetails: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDetails.text = labelDetails.text?.stringByReplacingOccurrencesOfString("PATIENT_NAME", withString: patient.fullName)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func buttonOptionSelection(sender: UIButton) {
        sender.selected = !sender.selected
    }
    
    @IBAction func buttonNextAction() {
        patient.boneGraftIsSelf = buttonIsSelf.selected
        patient.boneGraftIsSynthetic = buttonIsSynthetic.selected
        patient.boneGraftIsDonorHuman = buttonIsDonorHuman.selected
        
        let step2VC = self.storyboard?.instantiateViewControllerWithIdentifier("kBoneGraftInformedConsent2VC") as! BoneGraftInformedConsent2VC
//        step2VC.patient = patient
        self.navigationController?.pushViewController(step2VC, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
