//
//  BoneGraftInformedConsentFormVC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 01/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class BoneGraftInformedConsentFormVC: PDViewController {

    @IBOutlet weak var buttonIsSelf: UIButton!
    @IBOutlet weak var buttonIsDonorHuman: UIButton!
    @IBOutlet weak var buttonIsSynthetic: UIButton!
    @IBOutlet weak var labelName: UILabel!
    
    @IBOutlet weak var labelDate1: UILabel!
    @IBOutlet weak var labelDate2: UILabel!
    @IBOutlet weak var parentSignatureView: UIImageView!
    @IBOutlet weak var signatureWitnessName: UIImageView!
    @IBOutlet weak var dentistSignatureView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonIsSelf.selected = patient.boneGraftIsSelf
        buttonIsDonorHuman.selected = patient.boneGraftIsDonorHuman
        buttonIsSynthetic.selected = patient.boneGraftIsSynthetic
        labelName.text = patient.fullName
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        parentSignatureView.image = patient.boneGraftPatientSignature
        signatureWitnessName.image = patient.boneGraftWitness
        dentistSignatureView.image = patient.boneGraftDentistSignature
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
