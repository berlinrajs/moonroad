//
//  BoneGraftInformedConsent2VC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 01/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class BoneGraftInformedConsent2VC: PDViewController {

    @IBOutlet weak var labelDate1: DateLabel!
    @IBOutlet weak var labelDate2: DateLabel!
    @IBOutlet weak var parentSignatureView: SignatureView!
    @IBOutlet weak var signatureWitness: SignatureView!
    @IBOutlet weak var dentistSignatureView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate2.todayDate = patient.dateToday
        labelDate1.todayDate = patient.dateToday
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        if !parentSignatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate1.dateTapped || !labelDate2.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        } else if !signatureWitness.isSigned() {
            self.showAlert("PLEASE GET SIGN FROM WITNESS")
        } else {
            patient.boneGraftPatientSignature = parentSignatureView.signatureImage()
            patient.boneGraftDentistSignature = dentistSignatureView.image
            patient.boneGraftWitness = signatureWitness.signatureImage()
            
            let formVC = self.storyboard?.instantiateViewControllerWithIdentifier("kBoneGraftInformedConsentFormVC") as! BoneGraftInformedConsentFormVC
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
}
