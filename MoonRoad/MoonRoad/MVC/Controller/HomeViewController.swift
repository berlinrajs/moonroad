//
//  HomeViewController.swift
//  AdultMedicalForm
//
//  Created by SRS Web Solutions on 06/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class HomeViewController: PDViewController {
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var tablevViewForm: UITableView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var viewAlert: PDView!
    @IBOutlet weak var labelVersion: UILabel!
    var consentIndex : Int = 0
    var selectedForms : [Forms]! = [Forms]()
    var formList : [Forms]! = [Forms]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let text = NSBundle.mainBundle().infoDictionary?[kCFBundleVersionKey as String] as? String {
            labelVersion.text = text
        }
        self.navigationController?.navigationBar.hidden = true
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HomeViewController.showAlert1), name: kFormsCompletedNotification, object: nil)
                NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(dateChangedNotification), name: kDateChangedNotification, object: nil)
                self.dateChangedNotification()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        Forms.getAllForms { (isConnectionFailed, forms) -> Void in
            self.formList = forms
            self.tablevViewForm.reloadData()
            if isConnectionFailed == true {
                let alertController = UIAlertController(title: "MOON ROAD DENTISTRY", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.Alert)
                let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.sharedApplication().openURL(url)
                    }
                }
                let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                    
                }
                alertController.addAction(alertOkAction)
                alertController.addAction(alertCancelAction)
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func dateChangedNotification() {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        self.lblDate.text = dateFormatter.stringFromDate(NSDate()).uppercaseString
    }
    
    
    func showAlert1() {
        viewAlert.hidden = false
        viewShadow.hidden = false
    }
    
    @IBAction func buttonActionOk(sender: AnyObject) {
        viewAlert.hidden = true
        viewShadow.hidden = true
    }
    
    @IBAction func btnNextAction(sender: AnyObject) {
        selectedForms.removeAll()
        for (_, form) in formList.enumerate() {
            if form.isSelected == true {
                if form.formTitle == kConsentForms  {
                    for subForm in form.subForms {
                        if subForm.isSelected == true {
                            selectedForms.append(subForm)
                        }
                    }
                } else {
                    selectedForms.append(form)
                }
            }
        }
        self.view.endEditing(true)
        if selectedForms.count > 0 {
            selectedForms.sortInPlace({ (formObj1, formObj2) -> Bool in
                return formObj1.index < formObj2.index
            })
            patient = PDPatient(forms: selectedForms)
            patient.dateToday = lblDate.text
            let patientInfoVC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientInfoVC") as! PatientInfoViewController
//            patientInfoVC.patient = patient
            self.navigationController?.pushViewController(patientInfoVC, animated: true)
        } else {
            self.showAlert("PLEASE SELECT ANY FORM")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension HomeViewController : UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == consentIndex {
            let subForms = formList[consentIndex].subForms
            for subFrom in subForms {
                subFrom.isSelected = false
            }
        }
        
        var form : Forms!
//        if (indexPath.row <= consentIndex) {
//            
////            tableView.reloadData()
//        } else {
//            form = formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
//        }
        form = indexPath.row <= consentIndex ? formList[indexPath.row] : formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
        form.isSelected = !form.isSelected!
        
        if indexPath.row <= consentIndex {
            var indexPaths : [NSIndexPath] = [NSIndexPath]()
            for (idx, _) in form.subForms.enumerate() {
                let indexPath = NSIndexPath(forRow: 1 + idx, inSection: 0)
                indexPaths.append(indexPath)
            }
            if form.isSelected == true {
                tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: .Bottom)
                let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.2 * Double(NSEC_PER_SEC)))
                dispatch_after(delayTime, dispatch_get_main_queue()) {
                    if form.isSelected == true {
                        tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: indexPaths.last!.row, inSection:  0), atScrollPosition: .Bottom, animated: true)
                    }
                }
            } else {
                tableView.deleteRowsAtIndexPaths(indexPaths, withRowAnimation: .Bottom)
            }
            tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
        } else  {
            tableView.reloadData()
        }
        
        if form.formTitle == kDentalSealant && form.isSelected == true {
            PopupTextField.popUpView().showWithTitle("PLEASE ENTER THE TOOTH NUMBERS", placeHolder: "01, 15, 26, 30", keyboardType: UIKeyboardType.NumbersAndPunctuation, textFormat: TextFormat.ToothNumber, inViewController: self, completion: { (popUpView, textField, isEdited) in
                if textField.isEmpty {
                    form.isSelected = false
                    form.toothNumbers = nil
                    self.tablevViewForm.reloadData()
                } else {
                    form.toothNumbers = textField.text!
                }
            })
        } else if form.isToothNumberRequired == true {
            form.toothNumbers = nil
        }
        if (form.formTitle == kRootCanal && form.isSelected == true) ||  (form.formTitle == kSurgicalProcedure && form.isSelected == true){
            RootCanalAdditionalView.popUpView().show(self, completion: { (popUpView, toothNumbers, condition) in
                if toothNumbers == nil || condition == nil {
                    form.isSelected = false
                    form.toothNumbers = nil
                    form.additionalDetails = nil
                    self.tablevViewForm.reloadData()
                } else {
                    form.toothNumbers = toothNumbers!
                    form.additionalDetails = condition
                }
            })
        } else if form.formTitle == kRootCanal {
            form.toothNumbers = nil
            form.additionalDetails = nil
        }
        if form.formTitle == kHomeWhitening && form.isSelected == true {
            TeethWhiteningHoursView.popUpView().show(self, completion: { (popUpView, hour1, hour2) in
                if hour1 == nil || hour2 == nil {
                    form.isSelected = false
                    form.toothNumbers = nil
                    form.additionalDetails = nil
                    self.tablevViewForm.reloadData()
                } else {
                    form.toothNumbers = hour1!
                    form.additionalDetails = hour2!
                }
            })
        } else if form.formTitle == kHomeWhitening {
            form.toothNumbers = nil
            form.additionalDetails = nil
        }
        if form.formTitle == kEtiology && form.isSelected == true {
            PopupTextField.popUpView().showWithTitle("PLEASE ENTER THE TOOTH NUMBERS", placeHolder: "01, 15, 26, 30", keyboardType: UIKeyboardType.NumbersAndPunctuation, textFormat: TextFormat.ToothNumber, inViewController: self, completion: { (popUpView, textField, isEdited) in
                if textField.isEmpty {
                    form.isSelected = false
                    form.toothNumbers = nil
                    self.tablevViewForm.reloadData()
                } else {
                    form.toothNumbers = textField.text!
                }
            })
        } else if form.isToothNumberRequired == true {
            form.toothNumbers = nil
        }
    }
}

extension HomeViewController : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if formList.count > 0 {
            let consentForm = formList[consentIndex]
            return consentForm.isSelected == true ? formList.count + formList[consentIndex].subForms.count  : formList.count
        } else {
            return 0
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return indexPath.row == 27 || indexPath.row == 25 || indexPath.row == 22 || indexPath.row == 1 ? 65 : 40
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if (indexPath.row <= 0) {
            let cell = tableView.dequeueReusableCellWithIdentifier("CellMainForm", forIndexPath: indexPath) as! HomePageTableViewCell
            let form = formList[indexPath.row]
            cell.lblName.text = form.formTitle
            cell.imgViewCheckMark.hidden = !form.isSelected
            cell.backgroundColor = UIColor.clearColor()
            cell.contentView.backgroundColor = UIColor.clearColor()
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("CellSubForm", forIndexPath: indexPath) as! HomePageTableViewCell
            let form = formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
            cell.lblName.text = form.formTitle
            cell.imgViewCheckMark.hidden = !form.isSelected
            cell.backgroundColor = UIColor.clearColor()
            cell.contentView.backgroundColor = UIColor.clearColor()
            return cell
        }
    }
    
}




