//
//  ConsentRelineFormVC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 05/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ConsentRelineFormVC: PDViewController {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelDateOfBirth: UILabel!
    @IBOutlet weak var signatureView: UIImageView!
    @IBOutlet weak var witnessSignatureView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelName.text = patient.fullName
        labelDate.text = patient.dateToday
        labelDateOfBirth.text = patient.dateOfBirth
        signatureView.image = patient.relineSignature
        witnessSignatureView.image = patient.relineWitnessSignature
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
