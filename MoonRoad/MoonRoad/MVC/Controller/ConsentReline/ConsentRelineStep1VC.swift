//
//  ConsentRelineStep1VC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 05/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ConsentRelineStep1VC: PDViewController {

    @IBOutlet weak var patientSignatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var witnessSignatureView: SignatureView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonNextAction() {
        if !patientSignatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        } else if !witnessSignatureView.isSigned() {
            self.showAlert("PLEASE GET SIGN FROM WITNESS")
        } else {
            patient.relineSignature = patientSignatureView.signatureImage()
            patient.relineWitnessSignature = witnessSignatureView.signatureImage()
            
            let formVC = self.storyboard?.instantiateViewControllerWithIdentifier("kConsentRelineFormVC") as! ConsentRelineFormVC
//            formVC.patient = patient
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
}
//extension ConsentRelineStep1VC: UITextFieldDelegate {
//    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
//        if textField == textFieldWitness {
//            return textField.formatFullName(range, string: string)
//        }
//        return true
//    }
//    func textFieldShouldReturn(textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
//        return true
//    }
//}
