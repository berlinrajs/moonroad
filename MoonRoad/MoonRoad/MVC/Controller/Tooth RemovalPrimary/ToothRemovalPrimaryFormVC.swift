//
//  ToothRemovalPrimaryFormVC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 05/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ToothRemovalPrimaryFormVC: PDViewController {

    @IBOutlet weak var signatureView: UIImageView!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var signatureWitness: UIImageView!
    @IBOutlet weak var labelToothNumber: UILabel!
    @IBOutlet weak var labelName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelName.text = patient.fullName
        signatureView.image = patient.toothRemovalPrimarySignature
        labelDate.text = patient.dateToday
        labelToothNumber.text = patient.toothRemovalPrimaryTooth
        signatureWitness.image = patient.toothRemovalPrimaryWitness
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
