//
//  ToothRemovalPrimaryStep1VC.swift
//  MoonRoad
//
//  Created by SRS Web Solutions on 05/07/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ToothRemovalPrimaryStep1VC: PDViewController {

    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var signatureWitness: SignatureView!
    @IBOutlet weak var textFieldToothNumber: PDTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonNextAction() {
        if !signatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        } else if !signatureWitness.isSigned() {
            self.showAlert("PLEASE GET SIGN FROM WITNESS")
        } else if textFieldToothNumber.isEmpty {
            self.showAlert("PLEASE ENTER THE TOOTH NUMBER")
        } else {
            patient.toothRemovalPrimarySignature = signatureView.signatureImage()
            patient.toothRemovalPrimaryTooth = textFieldToothNumber.isEmpty ? "" : textFieldToothNumber.text!
            patient.toothRemovalPrimaryWitness = signatureWitness.signatureImage()
            
            let formVC = self.storyboard?.instantiateViewControllerWithIdentifier("kToothRemovalPrimaryFormVC") as! ToothRemovalPrimaryFormVC
//            formVC.patient = self.patient
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
}
extension ToothRemovalPrimaryStep1VC: UITextFieldDelegate {
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldToothNumber {
            return textField.formatToothNumbers(range, string: string)
        }
        return true
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
