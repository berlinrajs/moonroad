//
//  PeriodontalFormViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 7/5/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PeriodontalFormViewController: PDViewController {

    var patientSignature : UIImage!
    var witnessSignature : UIImage!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var patientSignatureView : UIImageView!
    @IBOutlet weak var witnessSignatureView : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelPatientName.text = patient.fullName
        labelDate.text = patient.dateToday
        patientSignatureView.image = patientSignature
        witnessSignatureView.image = witnessSignature
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
