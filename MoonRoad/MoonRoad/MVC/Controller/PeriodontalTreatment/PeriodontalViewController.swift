//
//  PeriodontalViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 7/4/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PeriodontalViewController: PDViewController {
    

    @IBOutlet weak var signatureViewPatient : SignatureView!
    @IBOutlet weak var signatureViewWitness : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onNextButtonPressed (sender : UIButton){
        if !signatureViewPatient.isSigned() || !signatureViewWitness.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }
        else{
            let new1VC = newStoryBoard.instantiateViewControllerWithIdentifier("PeriodontalFormVC") as! PeriodontalFormViewController
            new1VC.patientSignature = signatureViewPatient.signatureImage()
            new1VC.witnessSignature = signatureViewWitness.signatureImage()
//            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)

        }

    }

}
