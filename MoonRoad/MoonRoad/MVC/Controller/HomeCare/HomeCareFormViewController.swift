//
//  HomeCareFormViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 7/1/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class HomeCareFormViewController: PDViewController {

    @IBOutlet weak var labelDriverName : UILabel!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var signatureDriver : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDriverName.text = patient.homeCareDriverName
        labelPatientName.text = patient.fullName
        signatureDriver.image = patient.homeCareSignature
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
