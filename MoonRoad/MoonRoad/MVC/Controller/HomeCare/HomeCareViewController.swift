//
//  HomeCareViewController.swift
//  MoonRoad
//
//  Created by Bala Murugan on 7/1/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class HomeCareViewController: PDViewController {

    @IBOutlet weak var textfieldDriverName : UITextField!
    @IBOutlet weak var signature : SignatureView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (sender : UIButton){
        if !signature.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        } else if textfieldDriverName.isEmpty{
            self.showAlert("PLEASE ENTER THE DRIVER NAME")
        } else{
            patient.homeCareSignature = signature.signatureImage()
            patient.homeCareDriverName = textfieldDriverName.text!
            let new1VC = self.storyboard!.instantiateViewControllerWithIdentifier("HomeCareFormVC") as! HomeCareFormViewController
//            new1VC.patient = self.patient
            self.navigationController?.pushViewController(new1VC, animated: true)
        }
    }
}

extension HomeCareViewController : UITextFieldDelegate{
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
