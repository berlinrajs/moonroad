//
//  PatientInfoViewController.swift
//  ProDental
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientInfoViewController: PDViewController {
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var textFieldFirstName: PDTextField!
    @IBOutlet weak var textFieldLastName: PDTextField!
    @IBOutlet weak var textFieldDateOfBirth: PDTextField!
    @IBOutlet weak var textFieldInitial: PDTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DateInputView.addDatePickerForTextField(textFieldDateOfBirth, minimumDate: nil, maximumDate: NSDate())

        labelDate.text = patient.dateToday
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionNext(sender : AnyObject) {
        self.view.endEditing(true)
        if textFieldFirstName.isEmpty {
            self.showAlert("PLEASE ENTER FIRST NAME")
        } else if textFieldLastName.isEmpty {
            self.showAlert("PLEASE ENTER LAST NAME")
        } else if textFieldDateOfBirth.isEmpty {
            self.showAlert("PLEASE ENTER DATE OF BIRTH")
            
        } else {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"
            let date = dateFormatter.dateFromString(textFieldDateOfBirth.text!.capitalizedString)
            if date == nil {
                self.showAlert("PLEASE ENTER VALID DATE")
            } else {
                patient.firstName = textFieldFirstName.text
                patient.lastName = textFieldLastName.text
                patient.dateOfBirth = textFieldDateOfBirth.text
                patient.initial = textFieldInitial.isEmpty ? "" : textFieldInitial.text
                self.gotoNextForm(true)
            }
        }
    }
    
    @IBAction func buttonActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
}


extension PatientInfoViewController : UITextFieldDelegate{
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldInitial {
            return textField.formatInitial(range, string: string)
        } else if textField == textFieldFirstName || textField == textFieldLastName {
            return textField.formatName(range, string: string)
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
