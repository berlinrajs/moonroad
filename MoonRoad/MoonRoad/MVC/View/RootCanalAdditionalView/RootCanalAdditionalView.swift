//
//  RootCanalAdditionalView.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/12/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class RootCanalAdditionalView: UIView {

    static let sharedInstance = NSBundle.mainBundle().loadNibNamed("RootCanalAdditionalView", owner: nil, options: nil)!.first as! RootCanalAdditionalView
    
    class func popUpView() -> RootCanalAdditionalView {
        return NSBundle.mainBundle().loadNibNamed("RootCanalAdditionalView", owner: nil, options: nil)!.first as! RootCanalAdditionalView
    }
    
    @IBOutlet weak var textFieldToothNumbers: PDTextField!
    @IBOutlet weak var textFieldCondition: PDTextField!
    
    var completion:((RootCanalAdditionalView, String?, String?)->Void)?
    
    func show(inViewController: UIViewController?, completion : (popUpView: RootCanalAdditionalView, toothNumbers : String?, condition : String?) -> Void) {
        textFieldToothNumbers.text = ""
        
        self.completion = completion
        if inViewController == nil {
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.window!.addSubview(self)
        } else {
            inViewController!.view.addSubview(self)
        }
        
        self.subviews[0].transform = CGAffineTransformMakeScale(0.1, 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
    
    func close() {
        self.removeFromSuperview()
    }
    
    @IBAction func buttonActionOK(sender: AnyObject) {
        self.completion?(self, self.textFieldToothNumbers.isEmpty ? nil : self.textFieldToothNumbers.text!, self.textFieldCondition.isEmpty ? nil : self.textFieldCondition.text!)
        self.removeFromSuperview()
    }
}


extension RootCanalAdditionalView : UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldToothNumbers {
            return textField.formatToothNumbers(range, string: string)
        }
        return true
    }
    
    
}
