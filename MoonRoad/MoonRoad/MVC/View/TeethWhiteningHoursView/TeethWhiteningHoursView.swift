//
//  TeethWhiteningHoursView.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/12/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class TeethWhiteningHoursView: UIView {

    static let sharedInstance = NSBundle.mainBundle().loadNibNamed("TeethWhiteningHoursView", owner: nil, options: nil)!.first as! TeethWhiteningHoursView
    
    class func popUpView() -> TeethWhiteningHoursView {
        return NSBundle.mainBundle().loadNibNamed("TeethWhiteningHoursView", owner: nil, options: nil)!.first as! TeethWhiteningHoursView
    }
    
    @IBOutlet weak var textFieldHour1: PDTextField!
    @IBOutlet weak var textFieldHour2: PDTextField!
    
    var completion:((TeethWhiteningHoursView, String?, String?)->Void)?
    
    func show(inViewController: UIViewController?, completion : (popUpView: TeethWhiteningHoursView, hour1 : String?, hour2 : String?) -> Void) {
        textFieldHour1.text = ""
        textFieldHour2.text = ""
        
        self.completion = completion
        if inViewController == nil {
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.window!.addSubview(self)
        } else {
            inViewController!.view.addSubview(self)
        }
        
        self.subviews[0].transform = CGAffineTransformMakeScale(0.1, 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransformIdentity
        UIView.commitAnimations()
    }
    
    func close() {
        self.removeFromSuperview()
    }
    
    @IBAction func buttonActionOK(sender: AnyObject) {
        self.completion?(self, self.textFieldHour1.isEmpty ? nil : self.textFieldHour1.text!, self.textFieldHour2.isEmpty ? nil : self.textFieldHour2.text!)
        self.removeFromSuperview()
    }
}


extension TeethWhiteningHoursView : UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if string.rangeOfCharacterFromSet(NSCharacterSet(charactersInString: "01234567890").invertedSet)?.last != nil {
            return false
        }
        let newRange = textField.text!.startIndex.advancedBy(range.location)..<textField.text!.startIndex.advancedBy(range.location + range.length)
        let newString = textField.text!.stringByReplacingCharactersInRange(newRange, withString: string)
        if newString.characters.count > 2 {
            return false
        }
        if Int(newString) > 24 {
            return false
        }
        return true
    }
}
