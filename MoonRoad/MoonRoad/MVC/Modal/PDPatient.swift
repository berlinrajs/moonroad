//
//  PDPatient.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


class PDPatient: NSObject {
    var selectedForms : [Forms]!
    var firstName : String!
    var lastName : String!
    var initial : String?
    var dateOfBirth : String!
    var dateToday : String!


    var fullName: String {
        return initial!.characters.count > 0 ? firstName + " " + initial! + " " + lastName : firstName + " " + lastName
    }
    
    required override init() {
        super.init()
    }
    
    init(forms : [Forms]) {
        super.init()
        self.selectedForms = forms
    }
    
    var oralSedationDriverName : String!
    var oralSedationPatientSignature : UIImage!
    var oralSedationWitnessSignature : UIImage!
    
    var mouthGaurdParentSignature : UIImage!
    var mouthGaurdWitnessSignature : UIImage!
    
    var restorativeToothNumber : String!
    var restorativeShade : String!
    var restorativePatientSign : UIImage!
    var restorativeWitnessSign : UIImage!
    
    var airwaySignaturePatient : UIImage!
    var airwaySignatureDoctor : UIImage!

    var bridgeSignaturePatient : UIImage!
    var bridgeSignatureDoctor : UIImage!
    
    var intravenousSignaturePatient : UIImage!
    var intravenousSignatureWitness : UIImage!
    
    var toothRemovalSignaturePatient : UIImage!
    var toothRemovalSignatureWitness : UIImage!
    
    var inhalationSignaturePatient : UIImage!
    var inhalationSignatureWitness : UIImage!

    var homeCareDriverName : String!
    var homeCareSignature : UIImage!

    
    var crownPrepSignature: UIImage!
    var crownPrepCheckList: [PDQuestion]!
    
    var boneGraftIsSelf: Bool!
    var boneGraftIsDonorHuman: Bool!
    var boneGraftIsSynthetic: Bool!
    var boneGraftPatientSignature: UIImage!
    var boneGraftDentistSignature: UIImage!
    var boneGraftWitness: UIImage!
    
    var dentalSealantPatientSign: UIImage!
    var dentalSealantWitness: UIImage!
    
    var rootCanalSelectedComplications: [Int]!
    var rootCanalSignture: UIImage!
    var rootCanalWitnessSign: UIImage!
    
    var orthodonticSignature: UIImage!
    
//    var prostheticsTreatmentOptions: [Int]!
    var prostheticsTreatmentSignature: UIImage!
    var prostheticsTreatmentWitness: UIImage!
    var prostheticsTreatmentToothNumber: String!
    var prostheticsTreatmentShade: String!
    var prostheticsInitials: NSMutableDictionary!
    
    var endosseousImplantPatientSign: UIImage!
    var endosseousImplantPatientInitial: UIImage!
    var endosseousImplantWitnessSign: UIImage!
    var endosseousImplantDentistSign: UIImage!
    var endosseousImplantRelationShip: String!
    var endosseousImplantRelationName: String!
    var endosseousImplantLiklihoodSelectedOption: Int!
    var endosseousImplantReason: String!
    
    var preSedationSignature: UIImage!
    
    var relineSignature: UIImage!
    var relineWitnessSignature: UIImage!
    
//    var prosthondonticOptions: [Int]!
    var prosthondonticPatientSignature: UIImage!
    var prosthondonticInitials: NSMutableDictionary!
    var prosthondonticShade: String!
    var prosthondonticType: String!
    var prosthondonticWitness: UIImage!
    
    var toothRemovalPrimarySignature: UIImage!
    var toothRemovalPrimaryWitness: UIImage!
    var toothRemovalPrimaryTooth: String!
    
    var temporomandibularPatientSign: UIImage!
    var temporomandibularWitnessSign: UIImage!
    
    var teethWhiteningSignature: UIImage!
    var visit : CompleteVisit = CompleteVisit()
    
}

class CompleteVisit: NSObject {
    var socialSecurityNumber : String!
    var emailAddress : String!
    var cellNumber : String!
    var phoneNumber : String!
    var address : String!
    var addressTag : Int!
    var timeOfAppointment : String!
    var reason : String!
    var insurance : String!
    var insuranceTag : Int!
    
    required override init() {
        super.init()
    }

}
