//
//  PDFManager.swift
//  FutureDentistry
//
//  Created by Office on 2/21/16.
//  Copyright © 2016 SRS. All rights reserved.
//
let kKeychainItemLoginName = "Moon Road Dentistry: Google Login"
let kKeychainItemName = "Moon Road Dentistry: Google Drive"
let kClientId = "768128640045-7i930alhspheds2i38h792k5ivt0jb5h.apps.googleusercontent.com"
let kClientSecret = "VozxqTNI3V8bOuENMTskyGnB"
private let kFolderName = "MoonRoadDentistry"

import UIKit

class PDFManager: NSObject, GIDSignInUIDelegate, GIDSignInDelegate {
    
    var service : GTLServiceDrive!
    var credentials : GTMOAuth2Authentication!
    var authViewController: UIViewController!
    var completion: ((success : Bool) -> Void)!
    
    private func driveService() -> GTLServiceDrive {
        if (service == nil)
        {
            service = GTLServiceDrive()
            service.shouldFetchNextPages = true
            service.retryEnabled = true
        }
        return service
    }
    
    func uploadToGoogleDrive(view : UIView, patient: PDPatient, completionBlock:(finished : Bool) -> Void) {
        
        if view.isKindOfClass(UIScrollView) {
            self.createPDFForScrollView(view as! UIScrollView, patient: patient, completionBlock: { (finished) in
                    completionBlock(finished: finished)
            })
        } else {
            self.createPDFForView(view, patient: patient, completionBlock: { (finished) in
                completionBlock(finished: finished)
            })
        }
    }
    
    func createImageForView(view : UIView, patient : PDPatient, completionBlock:(finished : Bool) -> Void) {
        var image: UIImage?
        UIGraphicsBeginImageContext(view.bounds.size)
        view.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if let _ = image {
            self.saveImage(UIImageJPEGRepresentation(image!, 1.0)!, patient: patient, completionBlock: completionBlock)
        }
    }
    
    func createImageForScrollView(scrollView : UIScrollView, patient : PDPatient, completionBlock:(finished : Bool) -> Void) {
        var image: UIImage?
        scrollView.scrollEnabled = false
        scrollView.clipsToBounds = false
        let size: CGSize = CGSizeMake(scrollView.contentSize.width, scrollView.contentSize.height)
        
        let savedContentOffset = scrollView.contentOffset
        
        UIGraphicsBeginImageContext(size)
        scrollView.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        
        image = UIGraphicsGetImageFromCurrentImageContext()
        scrollView.contentOffset = savedContentOffset
        
        UIGraphicsEndImageContext()
        if let _ = image {
            self.saveImage(UIImageJPEGRepresentation(image!, 1.0)!, patient: patient, completionBlock: completionBlock)
        }
        scrollView.scrollEnabled = true
        scrollView.clipsToBounds = true
    }
    
    
    func saveImage(imageData : NSData, patient : PDPatient, completionBlock:(finished : Bool) -> Void) {
        do {
            // save as a local file
            let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "MM'_'dd'_'yyyy"
            let dateString = dateFormatter.stringFromDate(NSDate()).uppercaseString
            let name = "\(patient.fullName.fileName)_\(dateString)_\(patient.selectedForms.first!.formTitle.fileName).jpg"
            let path = "\(documentsPath)/\(name)"
            try imageData.writeToFile(path, options: .DataWritingAtomic)
            self.uploadFileToDrive(path, fileName: name)
            completionBlock(finished: true)
        } catch _ as NSError {
            completionBlock(finished: false)
        }
    }
    
    func createPDFForView(view : UIView, patient : PDPatient, completionBlock:(finished : Bool) -> Void) {
        NSOperationQueue.mainQueue().addOperationWithBlock {
            let pdfData = NSMutableData()
            let pageSize = screenSize
            UIGraphicsBeginPDFContextToData(pdfData, CGRectZero, nil)
            let pdfContext : CGContextRef = UIGraphicsGetCurrentContext()!
            UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, pageSize.width, pageSize.height), nil)
            view.layer.renderInContext(pdfContext)
            UIGraphicsEndPDFContext()
            do {
                // save as a local file
                let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "MM'_'dd'_'yyyy"
                let dateString = dateFormatter.stringFromDate(NSDate()).uppercaseString
                let name = patient.fullName.fileName + "_" + dateString + "_" + patient.selectedForms.first!.formTitle!.fileName + ".pdf"
                let path = "\(documentsPath)/\(name)"
                try pdfData.writeToFile(path, options: .DataWritingAtomic)
                self.uploadFileToDrive(path, fileName: name)
                completionBlock(finished: true)
            } catch _ as NSError {
                completionBlock(finished: false)
            }
        }
    }
    
    func createPDFForScrollView(scrollView : UIScrollView, patient : PDPatient, completionBlock:(finished : Bool) -> Void) {
        NSOperationQueue.mainQueue().addOperationWithBlock {
            let pdfData = NSMutableData()
            let scrollHeight = scrollView.contentSize.height
            let rawNumberOfPages = scrollHeight / screenSize.height
            let numberOfPages = Int(ceil(rawNumberOfPages))
            var pageNumber = Int()
            let pageSize = screenSize
            scrollView.setContentOffset(CGPointMake(0, 0), animated: false)
            UIGraphicsBeginPDFContextToData(pdfData, CGRectZero, nil)
            let pdfContext : CGContextRef = UIGraphicsGetCurrentContext()!
            repeat {
                UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, pageSize.width, pageSize.height), nil)
                if pageNumber < 1 {
                    scrollView.layer.renderInContext(pdfContext)
                } else if pageNumber >= 1 {
                    let offsetForScroll = CGFloat(pageNumber) * screenSize.height
                    scrollView.setContentOffset(CGPointMake(0, offsetForScroll), animated: false)
                    CGContextTranslateCTM(UIGraphicsGetCurrentContext()!, 0, -offsetForScroll)
                    scrollView.layer.renderInContext(pdfContext)
                }
                pageNumber = pageNumber + 1
            }
                while pageNumber < numberOfPages
            UIGraphicsEndPDFContext()
            do {
                // save as a local file
                let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "MM'_'dd'_'yyyy"
                let dateString = dateFormatter.stringFromDate(NSDate()).uppercaseString
                let name = patient.fullName.fileName + "_" + dateString + "_" + patient.selectedForms.first!.formTitle.fileName + ".pdf"
                let path = "\(documentsPath)/\(name)"
                try pdfData.writeToFile(path, options: .DataWritingAtomic)
                self.uploadFileToDrive(path, fileName: name)
                completionBlock(finished: true)
            } catch _ as NSError {
                completionBlock(finished: false)
            }
        }
    }
    
    
    
    ///MARK:- CHECK DRIVE FREE SPACE
    func CheckGoogleDriveFreeSpace()  {
        let query : GTLQueryDrive = GTLQueryDrive.queryForAboutGet()
        query.fields = "storageQuota,user"
        _  = driveService().executeQuery(query) { (ticket, about, error) in
            if error == nil{
                let abt : GTLDriveAbout = about as! GTLDriveAbout
                let limitInGB : Int = Int(abt.storageQuota.limit.doubleValue/1024.0/1024.0/1024.0)
                let usageinGB : Int = Int(abt.storageQuota.usage.doubleValue/1024.0/1024.0/1024.0)
                let userEmail : String = abt.user.emailAddress
                
                //ALMOST FULL
                if usageinGB > limitInGB - 1{
                    self.showGoogleDriveErrorAlert()
                    
                }else if (usageinGB/limitInGB) * 100 > 80{
                    /// 80% OF DRIVE IS FULL
                    self.sendWarningEmail(userEmail)
                    
                }
            }else{
                print("Error \(error.localizedDescription)")
            }
            
        }
    }
    
    //MARK:- GOOGLE DRIVE ERROR ALERT
    func showGoogleDriveErrorAlert () {
        ((UIApplication.sharedApplication().delegate as! AppDelegate).window?.rootViewController as! UINavigationController).viewControllers.last!.showAlert("WARNING", message: "YOUR GOOGLE DRIVE IS ALMOST FULL")
    }
    
    //MARK:- SEND WARNING EMAIL
    func sendWarningEmail(toEmail : String){
        let message : SMTPMessage = SMTPMessage()
        message.from = "demo@srswebsolutions.com"
        message.to = toEmail
        message.host = "smtp.gmail.com"
        message.account = "demo@srswebsolutions.com"
        message.pwd = "Srsweb123#"
        
        message.subject = "Your Google drive almost Full"
        message.content = "<p>Hi,</p><p>Your google drive account is 80% full. So please clear some contents immediately</p><p>Thank you.</p>"
        message.send({ (messg, now, total) in
            
        }, success: { (messg) in
            print("Email sent")
        }, failure: { (messg, error) in
        })
        
    }
    
    
    
    func uploadFileToDrive(path : String, fileName : String) {
        
        self.CheckGoogleDriveFreeSpace()
        
        func uploadFile(identitifer : String) {
            let driveFile = GTLDriveFile()
            driveFile.mimeType = fileName.hasSuffix(".pdf") ? "application/pdf" : "image/jpeg"
            driveFile.originalFilename = "\(fileName)"
            driveFile.name = "\(fileName)"
            driveFile.parents = [identitifer]
            
            let uploadParameters = GTLUploadParameters(data: NSData(contentsOfFile: path)!, MIMEType: fileName.hasSuffix(".pdf") ? "application/pdf" : "image/jpeg")
            let query = GTLQueryDrive.queryForFilesCreateWithObject(driveFile, uploadParameters: uploadParameters)
            query.addParents = identitifer
            
            self.driveService().executeQuery(query, completionHandler: { (ticket, uploadedFile, error) -> Void in
                if (error == nil) {
                    let fileManager = NSFileManager.defaultManager()
                    if fileManager.fileExistsAtPath(path) {
                        do {
                            try fileManager.removeItemAtPath(path)
                        } catch  {
                            
                        }
                    }
                } else {
                }
            })
        }
        
        func createFolder(folderName : String, parent : [String]?) {
            let folderObj = GTLDriveFile()
            folderObj.name = folderName
            if parent != nil {
                folderObj.parents = parent
            }
            folderObj.mimeType = "application/vnd.google-apps.folder"
            
            let queryFolder = GTLQueryDrive.queryForFilesCreateWithObject(folderObj, uploadParameters: nil)
            
            self.driveService().executeQuery(queryFolder, completionHandler: { (ticket, result, error) -> Void in
                if (error == nil) {
                    let folder = result as! GTLDriveFile
                    if parent == nil {
                        checkAndCreateSubFolder(folder)
                    } else {
                        uploadFile(folder.identifier)
                    }
                } else {
                    
                }
            })
        }
        
        func checkAndCreateSubFolder(folder : GTLDriveFile) {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "MM'-'dd'-'yyyy"
            let folderName = dateFormatter.stringFromDate(NSDate()).uppercaseString
            
            let folderDateQuery = GTLQueryDrive.queryForFilesList()
            folderDateQuery.q = "mimeType = 'application/vnd.google-apps.folder' and name = '\(folderName)' and trashed = false and '\(folder.identifier)' in parents"
            
            self.driveService().executeQuery(folderDateQuery, completionHandler: { (ticket, obj, error) -> Void in
                if error == nil {
                    let childFolder = (obj as! GTLDriveFileList).files
                    if childFolder != nil && childFolder.count > 0 {
                        let dateFolder = childFolder[0] as! GTLDriveFile
                        uploadFile(dateFolder.identifier)
                    } else {
                        createFolder(folderName, parent: [folder.identifier])
                    }
                } else {
                }
            })
        }
        
        let folderQuery = GTLQueryDrive.queryForFilesList()
        folderQuery.q = "mimeType = 'application/vnd.google-apps.folder' and trashed = false"
        
        self.driveService().executeQuery(folderQuery, completionHandler: { (ticker, folder, error) -> Void in
            if (error == nil) {
                let fileList = (folder as! GTLDriveFileList).files
                let name = "\(UIDevice.currentDevice().name)_" + kFolderName
                if fileList != nil {
                    let arrayFiltered = fileList.filter({ (driveFolders) -> Bool in
                        let folder = driveFolders as! GTLDriveFile
                        return folder.name == name
                    })
                    if arrayFiltered.count > 0 {
                        let folder = arrayFiltered[0] as! GTLDriveFile
                        checkAndCreateSubFolder(folder)
                        
                    } else {
                        createFolder(name, parent: nil)
                    }
                } else {
                    createFolder(name, parent: nil)
                }
            } else {
            }
        })
    }
    func authorizeDrive(viewController : UIViewController, completion:(success : Bool) -> Void) {
        credentials = GTMOAuth2ViewControllerTouch.authForGoogleFromKeychainForName(kKeychainItemName, clientID: kClientId, clientSecret: kClientSecret)
        if credentials.canAuthorize {
            self.driveService().authorizer = credentials
            completion(success: true)
        } else {
            self.completion = completion
            self.authViewController = viewController
            var configureError: NSError?
            GGLContext.sharedInstance().configureWithError(&configureError)
            assert(configureError == nil, "Error configuring Google services: \(configureError)")
            
            GIDSignIn.sharedInstance().uiDelegate = self
            GIDSignIn.sharedInstance().delegate = self
            GIDSignIn.sharedInstance().scopes = ["https://www.googleapis.com/auth/drive"]
            GIDSignIn.sharedInstance().signIn()
        }
    }
    
    func signIn(signIn: GIDSignIn!, presentViewController viewController: UIViewController!) {
        authViewController.presentViewController(viewController, animated: true, completion: nil)
    }
    
    func signIn(signIn: GIDSignIn!, dismissViewController viewController: UIViewController!) {
        authViewController.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!,
                withError error: NSError!) {
        if error == nil {
            // ...
            self.driveService().authorizer = user.authentication.fetcherAuthorizer()
            self.completion(success: true)
        } else {
            self.completion(success: false)
        }
    }
    
    func signIn(signIn: GIDSignIn!, didDisconnectWithUser user:GIDGoogleUser!,
                withError error: NSError!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
}
