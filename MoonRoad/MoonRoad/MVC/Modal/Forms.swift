//
//  Forms.swift
//  WestgateSmiles
//
//  Created by samadsyed on 2/18/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


let kFormsCompletedNotification = "kAllFormsCompletedSuccessfully"
let kDateChangedNotification = "kDateChangedToNextDateNotificaion"

let kConsentForms = "CONSENT FORMS"
let kAirwayAppliance = "INFORMED CONSENT FOR THE TREATMENT OF SNORING AND/OR OBSTRUCTIVE SLEEP APNEA WITH ORAL APPLIANCES"
let kBridgeAwareness = "INFORMED CONSENT FOR BRIDGE AWARENESS"
let kCrownPrep = "CROWN PREP CHECK LIST"
let kBoneGraft = "BONE GRAFT INFORMED CONSENT"
let kDentalSealant = "DENTAL SEALANT CONSENT FORM"
let kEndosseousImplant = "INFORMED CONSENT FOR ENDOSSEOUS IMPLANT"
let kRootCanal = "INFORMED CONSENT ENDODONTIC (ROOT CANAL) TREATMENT"
let kRestorativeTreatmment = "INFORMED CONSENT FOR RESTORATIVE TREATMENT"
let kIntravenousSedation = "INFORMED CONSENT FOR INTRAVENOUS SEDATION"
let kToothRemoval = "INFORMED CONSENT FOR IMPACTED TOOTH REMOVAL"
let kInhaledSedation = "INFORMED CONSENT FOR INHALATION SEDATION"
let kHomeCare = "IMMEDIATE DENTURES HOME CARE INSTRUCTIONS"
let kOrthodonticService = "COVERAGE OF ORTHODONTIC SERVICE­ HEALTH CHECK ONLY"
let kProstheticsTreatment = "INFORMED CONSENT PROSTHETICS TREATMENT - FIXED"
let kRetainerWear = "INSTRUCTIONS FOR RETAINER WEAR AND CARE"
let kPreSedation = "PRE-SEDATION INSTRUCTIONS"
let kCheckList = "IV CHECK LIST"
let kMouthGaurd = "INFORMED CONSENT FOR OCCLUSAL DEVICE"
let kOralSedation = "INFORMED CONSENT FOR ORAL SEDATION"
let kOralSurgery = "ORAL SURGERY POST-OPERATIVE INSTRUCTIONS"
let kBracesInstructions = "BRACES INSTRUCTIONS"
let kRootPlanning = "INFORMED CONSENT PERIODONTAL SCALING AND ROOT PLANNING"
let kPeriodontalTreatment = "INFORMED REFUSAL PERIODONTAL TREATMENT"
let kReline = "INFORMED CONSENT RELINE"
let kProsthodontic = "INFORMED CONSENT PROSTHODONTIC TREATMENT - REMOVABLE"
let kToothRemovalPrimary = "INFORMED CONSENT TOOTH REMOVAL/PRIMARY(BABY)"
let kTemporomandibular = "INFORMED CONSENT TEMPOROMANDIBULAR JOINT DYSFUNCTION TREATMENT"
let kCompleteVisit = "UPDATE PATIENT INFORMATION"
let kSurgicalProcedure = "INFORMED CONSENT SURGICAL PROCEDURE"
let kHomeWhitening = "INFORMED CONSENT HOME WHITENING"
let kEtiology = "ETIOLOGY FORM"
let kGingivectomy = "INFORMED CONSENT FOR GINGIVECTOMY"
let kToKnow = "WE WANT YOU TO KNOW (INFORMED CONSENT)"


//let toothNumbersRequired = [kDentalSealant]

class Forms: NSObject {
    
    var formTitle : String!
    var subForms : [Forms]!
    var isSelected : Bool!
    var index : Int!
    var toothNumbers : String!
    var isToothNumberRequired : Bool!
    var additionalDetails: String!

    init(formDetails : NSDictionary) {
        super.init()
        self.isSelected = false
    }
    
    override init() {
        super.init()
    }
    
    class func getAllForms (completion :(isConnectionfailed: Bool, forms : [Forms]?) -> Void) {
        let isConnected = Reachability.isConnectedToNetwork()
        let forms = [kConsentForms]
        let formObj = getFormObjects(forms, isSubForm: false)
        completion(isConnectionfailed: isConnected ? false : true, forms : formObj)
    }
    
    private class func getFormObjects (forms : [String], isSubForm : Bool) -> [Forms] {
        var formList : [Forms]! = [Forms]()
        for (idx, form) in forms.enumerate() {
            let formObj = Forms()
            formObj.isSelected = false
            formObj.index = isSubForm ? idx + 4 : idx
            formObj.formTitle = form
            formObj.isToothNumberRequired = false
            if formObj.formTitle == kConsentForms {
                formObj.subForms = getFormObjects([kAirwayAppliance,kBridgeAwareness,kCrownPrep, kBoneGraft, kDentalSealant,kEndosseousImplant, kRootCanal,kRestorativeTreatmment,kIntravenousSedation,kToothRemoval,kInhaledSedation,kHomeCare, kOrthodonticService, kProstheticsTreatment, kRetainerWear,kPreSedation, kCheckList, kMouthGaurd,kOralSedation,kOralSurgery,kBracesInstructions,kRootPlanning,kPeriodontalTreatment, kReline, kProsthodontic, kToothRemovalPrimary, kTemporomandibular,kCompleteVisit,kSurgicalProcedure, kHomeWhitening, kEtiology, kGingivectomy, kToKnow], isSubForm:  true)
            }
            formList.append(formObj)
        }
        return formList
    }
    
}
