//
//  YNTableView.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/31/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
public enum YNCellType: Int {
    case YesOrNo = 0, YesOrNoOrDK, CheckBox
}
protocol YNTableViewDataSource {
    func sectionTitlesInTableView(tableView: YNTableView) -> [String]?
    func arrayOfQuestionsInTableView(tableView: YNTableView) -> [[PDQuestion]]?
}

class YNTableView: UITableView, UITableViewDataSource, UITableViewDelegate {
    var delegateYNTableView : YNQuestionsTableViewCellDelegate?
    var cellType: YNCellType! {
        didSet {
            self.reloadData()
        }
    }
    
    var ynDataSource: YNTableViewDataSource! {
        didSet {
            self.arraySectionTitle = ynDataSource?.sectionTitlesInTableView(self)
            self.arrayOfQuestions = ynDataSource?.arrayOfQuestionsInTableView(self)
        }
    }
    
    var arrayOfQuestions : [[PDQuestion]]? {
        didSet {
            self.reloadData()
        }
    }
    var arraySectionTitle: [String]?
    
    override func awakeFromNib() {
        
        self.delegate = self
        self.dataSource = self
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return arrayOfQuestions != nil ? arrayOfQuestions!.count : 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfQuestions?[section] != nil ? arrayOfQuestions![section].count : 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("Cell") as? YNQuestionsTableViewCell
        if cell == nil {
            cell = YNQuestionsTableViewCell.initWith(self.cellType)
        }
        let obj = self.arrayOfQuestions![indexPath.section][indexPath.row]
        cell!.configCell(obj)
        cell!.tag = indexPath.row
        
        cell!.delegate = delegateYNTableView
        return cell!
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let obj = self.arrayOfQuestions![indexPath.section][indexPath.row]
        let height = obj.question.heightWithConstrainedWidth(self.frame.width - (cellType.rawValue == 0 ? 155 : cellType.rawValue == 1 ? 100 : 66), font: YNQuestionsTableViewCell.questionFont)
        return height + 10
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return arraySectionTitle == nil ? 0 : 30
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if arraySectionTitle == nil {
            return nil
        }
        
        var headerView = tableView.dequeueReusableHeaderFooterViewWithIdentifier("Header") as? YNHeaderView
        
        if headerView == nil {
            headerView = YNHeaderView(reuseIdentifier: "Header")
        }
        headerView!.configWithTitle(arraySectionTitle![section])
        
        return headerView
    }
}
class YNHeaderView: UITableViewHeaderFooterView {
    
    var labelHeader: UILabel!
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        let bgView = UIView()
        bgView.backgroundColor = UIColor.clearColor()
        self.backgroundView = bgView
        contentView.backgroundColor = UIColor.clearColor()
        
        labelHeader = UILabel(frame: CGRectZero)
        labelHeader.font = UIFont(name: "WorkSans-Regular", size: 15.0)
        labelHeader.backgroundColor = UIColor.clearColor()
        labelHeader.textColor = UIColor.whiteColor()
        labelHeader.textAlignment = NSTextAlignment.Left
        
//        labelHeader.numberOfLines = 0
        self.addSubview(labelHeader)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        labelHeader.frame = CGRectMake(0, 0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame))
    }
    func configWithTitle(title: String) {
        labelHeader.text = title
    }
}
