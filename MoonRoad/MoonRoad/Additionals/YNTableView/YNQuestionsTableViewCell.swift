//
//  YNQuestionsTableViewCell.swift
//  FutureDentistry
//
//  Created by SRS Web Solutions on 27/05/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

@objc protocol YNQuestionsTableViewCellDelegate {
    optional func questionAnswerRequired(forCell cell: YNQuestionsTableViewCell)
}

class YNQuestionsTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var radioButtonYes: RadioButton!
    @IBOutlet weak var buttonCheckBox: UIButton!
    var type: YNCellType!
    
    var question: PDQuestion!
    var delegate: YNQuestionsTableViewCellDelegate?
    
    static var questionFont: UIFont {
        get {
            return UIFont(name: "WorkSans-Regular", size: 14.0)!
        }
    }
    class func initWith(cellType: YNCellType) -> YNQuestionsTableViewCell? {
        var cell: YNQuestionsTableViewCell?
        switch  cellType {
        case .YesOrNo:
            cell = NSBundle.mainBundle().loadNibNamed("YNTableView", owner: nil, options: nil)![0] as? YNQuestionsTableViewCell
        case .YesOrNoOrDK:
            cell = NSBundle.mainBundle().loadNibNamed("YNTableView", owner: nil, options: nil)![1] as? YNQuestionsTableViewCell
        case .CheckBox:
            cell = NSBundle.mainBundle().loadNibNamed("YNTableView", owner: nil, options: nil)![2] as? YNQuestionsTableViewCell
        }
        cell!.type = cellType
        return cell
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configCell(question: PDQuestion) {
        self.backgroundColor = UIColor.clearColor()
        self.contentView.backgroundColor = UIColor.clearColor()
        
        self.question = question
        if self.type == YNCellType.YesOrNo {
            labelTitle.text = question.question
            if question.selectedOption == nil {
                radioButtonYes.deselectAllButtons()
            } else {
                radioButtonYes.setSelected(question.selectedOption)
            }
        } else if self.type == YNCellType.YesOrNoOrDK {
            labelTitle.text = question.question
            if question.selectedOption == nil {
                radioButtonYes.deselectAllButtons()
            } else {
                radioButtonYes.setSelected(question.selectedOption)
            }
        } else if self.type == YNCellType.CheckBox {
            labelTitle.text = question.question
            buttonCheckBox.setTitle("", forState: UIControlState.Normal)
            buttonCheckBox.selected = question.selectedOption
        }
    }
    
    @IBAction func radioButtonAction(sender: UIButton) {
        if self.type == YNCellType.CheckBox {
            sender.selected = !sender.selected
            question.selectedOption = sender.selected
            if question.isAnswerRequired == true && sender.selected {
                self.delegate?.questionAnswerRequired?(forCell: self)
            }
        }
        else {
            self.question.selectedOption = radioButtonYes.selected
            if question.isAnswerRequired == true && radioButtonYes.selected {
                self.delegate?.questionAnswerRequired?(forCell: self)
            }
        }
    }
}
